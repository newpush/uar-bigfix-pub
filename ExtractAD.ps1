<# 
.SYNOPSIS
COPYRIGHT BRAINWAVE, all rights reserved.
This computer program is protected by copyright law and international treaties.
Unauthorized duplication or distribution of this program, or any portion of it, may result in severe civil or criminal penalties, and will be prosecuted to the maximum extent possible under the law.

ExtractAD Version 5.2
Extract groups and user accounts of an Active Directory.
 
.DESCRIPTION
	USAGE :
	  ExtractAD.ps1 -filter<String> -attributesfile<String> -servers<String> -credential<String> -logLevel<String> -errAction<String> -printLog<boolean> -port<Int32> -authType<String> -useSSL<boolean> -outputDirectory<String> -logDirectory<String>
	  
	SCRIPT PARAMETERS:
	- filter : LDAP filter, default value is : "(|(objectcategory=Person)(objectclass=group))"
	- ouFilter : filter on a specific organizationalUnit, should be a valid organizationalUnit DN (example: OU=Internal Users,DC=intra,DC=test,DC=local)"
	- attributesfile : File that contains the attribute names to exclude or include in the extraction according to the mode parameter(by default, "attributes.cfg")
	- servers: File that contains the names of servers to do their extraction.
	- credential: file containing PSCredential used to extract rights on share and mount drive, see example to create and store PSCredential object to a file, if not set current session credential will be used.
	- logLevel: log level, possible values are: <'Debug','Info','Warning','Error'>, default value is Info
	- errAction: set error action  preference, possible values are: <'Continue','Ignore','Inquire','SilentlyContinue','Stop','Suspend'>, default value is 'Stop'
	- printLog: set to false to disable printing info on console, default value is 'True'
	- port: LDAP server port, default value is 389.
	- authType: authentication method to use in LDAP connection.
	- useSSL: boolean to activate SecureSocketLayer  on LDAP connection, default value is 'False'.
	- outputDirectory: directory where extracted file will be stored, if not set extracted file will be in execution directory.
	- logDirectory: directory where log file will be stored, if not set log file will be in execution directory.
	- hrData: set to true to export users only in csv file to be colected as hr data
	- customHrAttributes: Path to file that contains custom mapping between HR attributes and AD attributes (please check the documentation to create a valid file)

.EXAMPLE
./ExtractAD.ps1 -servers 'servers.cfg' -attributesfile 'attributes.cfg' -filter '(|(objectcategory=Person)(objectclass=organizationalUnit)(objectclass=group)(objectclass=container))'

.EXAMPLE
./ExtractAD.ps1 -servers 'servers.cfg'
	
- The script will extract all AD attributes.
- Default LDAP filter is '(|(objectcategory=Person)(objectclass=group))'

.EXAMPLE
./ExtractAD.ps1 -servers 'servers.cfg' -credential $credFile

.EXAMPLE
$password_ =  ConvertTo-SecureString –String $filerPassword –AsPlainText -Force		
$credential = New-Object –TypeName System.Management.Automation.PSCredential –ArgumentList $filerLogin, $password_ 
$credential | Export-CliXml credential.xml

Example to create PSCredential and store it in a file (password will be encrypted by powershell API), file can work only under local machine and user session where it was created.
		

.EXAMPLE
$credential = Get-Credential -UserName test\local		
$credential | Export-CliXml credential.xml

Example to create PSCredential in interactive mode and store it in a file (password will be encrypted by powershell API), file can work only under local machine and user session where it was created.

#>

# In connector mode: Parameters to send by the connector to the script
# [String]@config.filter LDAP filter
# [String]@config.ouFilter organizationalUnit DN filter
# [File]@config.attributesfile Attribute list
# [File]@config.serverList Server list
# [File]@config.credential Credential File on server
# [String]@config.logLevel  Log level
# [String]@config.errAction  Error action  preference
# [boolean]@config.printLog Print Logs in console
# [int]@config.port LDAP server port
# [String]@config.authType Authentication method
# [boolean]@config.useSSL activate SecureSocketLayer
# [String]@config.hrData extract RH data only
# [String]@config.customHrAttributes Custom HR attribute list

param
(
	[string]$attributesfile,
	[string]$servers,
	[string]$filter='(|(objectcategory=Person)(objectclass=group))',
	[string]$ouFilter,
	[string]$outputDirectory,
	[string]$logDirectory,
	[ValidateSet('Debug','Info','Warning','Error')][string]$logLevel = "Info",
    [ValidateSet('Continue','Ignore','Inquire','SilentlyContinue','Stop','Suspend')][string]$errAction = "Continue",
	[string]$printLog=$true,
	[string]$credential,
	[ValidateSet('Anonymous','Basic','Digest','Dpa','External','Kerberos','Msn','Negotiate','Ntlm')][string]$authType,
	$useSSL=$false,
	[boolean]$hrData=$false,
	$customHrAttributes,
	$memberlimit= 1000000
)

######################################## FUNCTIONS USED FOR LOGS ################################################

function Write-Log 
{ 
    [CmdletBinding()] 
    Param 
    ( 
        $Message,
        [ValidateSet("Error","Warning","Info","Debug")] [string]$Level="Info"
    )
	if(canLog $logLevel $Level){
		$formattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
		$formattedMsg = '[{0}] {1} - {2}' -f $formattedDate, $Level, $Message
		$formattedMsg >> $__logfile__
        if($Level -eq 'Error'){
            $Message >> $__logfile__
        }
		switch ($Level) { 
			'Error' { 
				printLog $formattedMsg $Level
			} 
			'Warning' { 
				printLog $formattedMsg $Level
			} 
			'Info' { 
				printLog $formattedMsg $Level
			}
			'Debug' { 
				printLog $formattedMsg $Level
			} 		
		} 	
	}
}

function printLog(){
    [CmdletBinding()] 
	Param 
    ( 
		[string]$message,
		[string]$Level="Info"
	)
    
	if($printLog -eq $true){
		if($connectorMode){
			send_message $Message $Level
		}else{
			Write-Host $Message
		}
	}
}

function canLog(){	
    [CmdletBinding()] 
	Param 
    ( 
		[string]$ScriptLogLevel,
		[string]$Level
	)
    $canLog = $false
	$Script = getLevel $ScriptLogLevel
	$curentLevel = getLevel $Level
	if($curentLevel -le $Script){
		$canLog = $true
	}
	$canLog
	return
}

function getLevel(){

    [CmdletBinding()] 
	Param 
    ( 
		[string]$Level
	)
	$numLevel = 0
	switch ($Level) { 
	    'Error' { 
		    $numLevel = 0
	    } 
	    'Warning' { 
		    $numLevel = 1
	    } 
	    'Info' { 
		    $numLevel = 2
	    }
	    'Debug' { 
		    $numLevel = 3
	    }
	}
	$numLevel
	return
}

#send_message
#print message in studio console if extractor launched in connector mode using studio
function send_message($msg,$level){
	#level can be: @Error	@Warning	@Log	@Info	@Debug	@Message
	if($connectorMode){		
		$d=(get-date).millisecond
		$result = @{"__UID__" = $script:connector_counter; "__NAME__" = $script:connector_counter;"__event_type__"="@$($level)";"__event_when__"="$d"; "__event_message__" = $msg;"__event_line__"="-1";"__event_script__"=$__SiloName__+"$"+ $__ConnectorName__+"$"+ $__ScriptName__}    	
		$script:connector_counter+=1
		if (!$Connector.Result.Process($result)) {					
			break
		}
	}
}

#send_signal
#send info to the host, ex: (data uncompleted, close streams)
#$signle: can be
#			"open" : to open a stream on host
#			"finish": to close a stream on host
#			"finish": generated output files on host are uncompleted (ex: extraction error)
#streamName: contain stream name, it must be used only if $signle is open or finish
function send_signal($signle, $streamName){
	#level can be: @Error	@Warning	@Log	@Info	@Debug	@Message
	if($connectorMode){		
		$result = @{"__UID__" = $script:connector_counter; "__NAME__" = $script:connector_counter;"__outputstrem_state__"=$signle;"__outputstrem_state_name__"=$streamName}    	
		$script:connector_counter+=1
		if (!$Connector.Result.Process($result)) {					
			break
		}
	}
}

# processCopy
# This function is used to send information using the connector mode
# Parameters
#     - $buf the buffer with the information to send
#     - $append true or false
function  processCopy ($buf, $append, $fileName) {
    $result = @{"__UID__" = $script:connector_counter; "__NAME__" = $script:connector_counter;"__APPEND__" = $append;"__bytesdata__"=$buf;"__outputstream__"=$fileName; "__outputtype__"="__bytes_copy__";"__bytesRead__" = $bytesRead;"__event_type__"="Data"}
    $script:connector_counter += 1
    if (!$Connector.Result.Process($result)) {					
    	break
    }
	
}

#send_log
#send log file to igrc in connector mode
function send_log() {
	try{
		$bufferSize = 65536
		$stream = [System.IO.File]::OpenRead($__logfile__)
		while ( $stream.Position -lt $stream.Length ) {
			#BEGIN CALLOUT A
			[byte[]]$buffer = new-object byte[] $bufferSize
			$bytesRead = $stream.Read($buffer, 0, $bufferSize)    
			processCopy $buffer $true $__logfile__   			
		 }
	}finally{
		 if($stream){
			$stream.Close()
		 }
	}
}

#send_file
#send data file to igrc in connector mode
function send_file($filepath) {
	try{
		$bufferSize = 65536
		$stream = [System.IO.File]::OpenRead($filepath)
		while ( $stream.Position -lt $stream.Length ) {
			[byte[]]$buffer = new-object byte[] $bufferSize
			$bytesRead = $stream.Read($buffer, 0, $bufferSize)    
			processCopy $buffer $true $filepath    			
		 }
	}finally{
		 if($stream){
			$stream.Close()
		 }
	}
}

function add_loginfo ( $message ) {
	$now_timestamp=get-Date
	$current_time=$now_timestamp.Year.ToString()+$now_timestamp.Month.ToString()+$now_timestamp.Day.ToString()+$now_timestamp.Hour.ToString()+$now_timestamp.Minute.ToString()+$now_timestamp.Second.ToString()+$now_timestamp.Millisecond.ToString()	
	$finalmessage = "[" + $current_time + "] " + $message
	$finalmessage >> $__logfile__
}

#Function overrideAttribures
#	Get attributes to extract using -attributesfile parameter if set, otherwise default attributes will be used, see help for more detail.
#	Parameters:
#		$default: default script attributes
function overrideAttribures(){	
	if ($attributesfile){
		if (test-path $attributesfile) {
			$cfg = Get-Content $attributesfile 
			foreach ($line in $cfg)	{
				if ($null -ne $line -and $line -ne "" -and ($false -eq $Script:attributesList.Contains($line))) {
					$line = $line.ToLower()
					$Script:attributesList += $line
				}
			}
		}else{
			$Script:extractADExitCode = 1
			throw "The file ($attributesfile) containing attribute list to export is not found"
		}
	}else{		
		Write-Log -message "No attributes file is set, the script will export default attributes" -Level Warning		
	}
}

function overrideHrAttribures(){
	$header = 'hr_attribute', 'ad_attribute'
	if ($customHrAttributes){
		if (test-path $customHrAttributes) {
			$cfg = Import-Csv -Path $customHrAttributes -Delimiter ';' -Header $header
			foreach ($line in $cfg)	{
				if ($null -ne $line){
					$hr_attribute = $line.hr_attribute
					$ad_attribute = $line.ad_attribute
					if ( $null -ne $hr_attribute -and "" -ne $hr_attribute -and $null -ne $ad_attribute -and "" -ne $ad_attribute) {
						if($script:hrAttributes.ContainsKey($hr_attribute)){
							$ad_attribute = $ad_attribute.ToLower()
							$script:hrAttributes[$hr_attribute] = $ad_attribute
							if ($false -eq $Script:attributesList.Contains($ad_attribute)) {
								$Script:attributesList += $ad_attribute
							}
						}
					}
				}
			}
		}else{
			$Script:extractADExitCode = 1
			throw "The file ($customHrAttributes) containing hr attributes mapping is not found"
		}
	}else{		
		Write-Log -message "No hr attributes mapping file is set, the script will export default mapping" -Level Warning		
	}
}

#Function getServerList
#	Get server list using -servers parameter
function getServerList(){
	$domains =@()
	if ($servers) {
		if (test-path $servers)	{
			$cfg = Get-Content $servers 
			foreach ($line in $cfg)	{
				if (($line -ne $null) -and ($line -ne "")) {
					$domains += $line
				}
			}
			if(($domains -eq $null) -or ($domains.Count -eq 0))	{
				Write-Log -Level Warning -message "The file ($servers) containing server list is empty"				
			}
		}else{
			$Script:extractADExitCode = 1
			throw "The file ($servers) containing server list not found"
		}
	}else{
		Write-Log "Server list not set, script will try to enumerate domains from current Forest" -Level Warning
		$CDomain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain()
		$Domains_objs = $CDomain.Forest.Domains		
		foreach ($objDomain_ in $Domains_objs)
		{	
			$domains+=$objDomain_.Name;
		}	
	}
	,$domains
	return
}

#Function handleAttribute
#	get value of an attributes from DirectoryAttribute object
#	Parameters:
#				$attribute: DirectoryAttribute
#				$attributeName: name of the attribute
function handleAttribute ([System.DirectoryServices.Protocols.DirectoryAttribute]$attribute, $attributeName){	
		if($attribute.Count -gt 0){		
		For ($i=0; $i -lt $attribute.Count; $i++) {
			$value = $attribute[$i]
			if ($attributeName -eq "objectsid" -or $attributeName -eq "sidhistory"){
				if($value.GetType().Name -eq "String"){
					$enc = [system.Text.Encoding]::UTF8
					$value = $enc.GetBytes($value)
					# Encoding.ASCII.GetBytes($value);
				}
				$value = (New-Object System.Security.Principal.SecurityIdentifier($value, 0)).Value
			}

			if ($attributeName -eq "objectguid"){
				$value = [System.GUID]$value
			}
			
			if($value.GetType().Tostring() -eq "System.Byte[]"){
				$stringBuilder = $stringBuilder.Append($attributeName).Append(":: ").AppendLine([System.Convert]::ToBase64String($value))
			}elseif (-not (isLDIFSafe $value)) {
				$enc = [system.Text.Encoding]::UTF8
				[Byte[]]$data = $enc.GetBytes($value)			
				$stringBuilder = $stringBuilder.Append($attributeName).Append(":: ").AppendLine([System.Convert]::ToBase64String($data))
			}else{
				$stringBuilder = $stringBuilder.Append($attributeName).Append(": ").AppendLine($value)
			}
		}
	}
}

Function getDomainInfo {
    Param (
        [System.DirectoryServices.Protocols.LdapConnection]$LdapConnection
    )
    $results = @()
    $propDef=@{"rootDomainNamingContext"=@(); "configurationNamingContext"=@(); "schemaNamingContext"=@();"defaultNamingContext"=@();"dnsHostName"=@()}

    #Get RootDSE info
    $rq=new-object System.DirectoryServices.Protocols.SearchRequest
    $rq.Scope = "Base"
    $rq.Attributes.AddRange($propDef.Keys) | Out-Null
    [System.DirectoryServices.Protocols.ExtendedDNControl]$exRqc = new-object System.DirectoryServices.Protocols.ExtendedDNControl("StandardString")
    $rq.Controls.Add($exRqc) | Out-Null
            
    $rsp=$LdapConnection.SendRequest($rq)
            
    $data=new-object PSObject -Property $propDef
            
    $data.configurationNamingContext = (($rsp.Entries[0].Attributes["configurationNamingContext"].GetValues([string]))[0]).Split(';')[1];
    $data.schemaNamingContext = (($rsp.Entries[0].Attributes["schemaNamingContext"].GetValues([string]))[0]).Split(';')[1];
    $data.rootDomainNamingContext = (($rsp.Entries[0].Attributes["rootDomainNamingContext"].GetValues([string]))[0]).Split(';')[2];
    $data.defaultNamingContext = (($rsp.Entries[0].Attributes["defaultNamingContext"].GetValues([string]))[0]).Split(';')[2];
    $data.dnsHostName = ($rsp.Entries[0].Attributes["dnsHostName"].GetValues([string]))[0]	
	
	$domainDN = $data.defaultNamingContext
	$results += $domainDN
	#Get netBiosName
	$rq2=new-object System.DirectoryServices.Protocols.SearchRequest
	$rq2.DistinguishedName = "CN=Partitions, $($data.configurationNamingContext)"
	$rq2.Attributes.Clear()
	$rq2.Attributes.Add("nETBIOSName")  | Out-Null
	$rq2.Filter = "(&(objectCategory=crossRef)(ncName= $($data.defaultNamingContext)))"
	$rqs2=$LdapConnection.SendRequest($rq2)

		if ($rqs2.Entries.Count -ge 0){		
			$foundItem = $rqs2.Entries[0]
			if($foundItem.Attributes["nETBIOSName"]){
				$netBios = $foundItem.Attributes["nETBIOSName"].GetValues([string])	
				$results += $netBios
			}
		}
	,$results
	return
  }
#Function handleGroupMembers
#	export group members using pagination if member count great then max (by default max if 1500)
#	Parameters:
#		$dn: disnguishedname of the group
#		$LDAPConnection: LDAP connection object
function handleGroupMembers($dn, [System.DirectoryServices.Protocols.LDAPConnection]$LDAPConnection){
	$attributeNameBuilder = New-Object -TypeName "System.Text.StringBuilder"
	$totalMemberCount = 0
	$pageSize = 1000
	$currentPageRange = 0;	
	$stillMembers = $true
	while($stillMembers){
		# Prepare the request
		$attributeNameBuilder = $attributeNameBuilder.Append("member;range=").Append($currentPageRange).Append("-").Append($currentPageRange + $pageSize -1)
		$memberRequest = New-Object System.directoryServices.Protocols.SearchRequest	
		$memberRequest.DistinguishedName = $dn
		#$memberRequest.DistinguishedName= $domainDN # Root
		$memberRequest.Filter = $filter
		$memberRequest.Scope = "Subtree"	
		$memberRequest.TimeLimit=(new-object System.Timespan(0,5,0))	
		[void]$memberRequest.Attributes.Clear()
		[void]$memberRequest.Attributes.AddRange($attributeNameBuilder.ToString())
    
		$response = $LdapConnection.SendRequest($memberRequest,(new-object System.Timespan(0,5,0)) ) -as [System.DirectoryServices.Protocols.SearchResponse];	    
		# Parse results
		$members = $response.Entries[0].Attributes[$attributeNameBuilder.ToString()]
		if($members.Count -lt $pageSize){
			# for last page attribute name will be member;range=xxx-* and not member;range=xx-yy
			$stillMembers = $false
			$attributeNameBuilder = $attributeNameBuilder.Clear()
			$attributeNameBuilder = $attributeNameBuilder.Append("member;range=").Append($currentPageRange).Append("-*")
			$members = $response.Entries[0].Attributes[$attributeNameBuilder.ToString()]
		}
		$totalMemberCount += $members.Count
		handleAttribute $members "member"		

		if($currentPageRange -gt $memberlimit){
			$stillMembers = $false
		}

		$attributeNameBuilder = $attributeNameBuilder.Clear()
		$currentPageRange +=  $pageSize
	}

	$groupMemberCountmsg = "{0} members exported for group {1}" -f $totalMemberCount ,$dn
	Write-Log -Message $groupMemberCountmsg -Level Debug
}

#Function addFileContent
#	append stringbuilder value to output ldif file
#	Parameters:
#		$filePath: ldif file
#		$buffer: StringBuilder containing bufferSize object
function addFileContent([System.String]$filePath, [System.Text.StringBuilder]$buffer){
	$streamWriter =  New-Object IO.StreamWriter -Arg $filePath,$true,([System.Text.Encoding]::UTF8)
	$streamWriter.WriteLine($buffer)
	$streamWriter.Close()	
}

#Function handleDomain
#	iterate on objects of a domain
#	parameers:
#		$Domain: domain name
#		$attributesList attributes to export
function handleDomain($Domain){

	$LDAPConnection = New-Object System.DirectoryServices.Protocols.LDAPConnection ($Domain <#$domainController#> )
	$LDAPConnection.SessionOptions.ProtocolVersion=3
	$LDAPConnection.SessionOptions.ReferralChasing="None"
	$LDAPConnection.Timeout=(new-object System.Timespan(10,0,0))
	if($exportCredential){$LDAPConnection.Credential = $exportCredential}
	if($authType){$LDAPConnection.AuthType = $authType}
	
	if($useSSL -eq $true){$LDAPConnection.SessionOptions.SecureSocketLayer = $true}

	#get netBiosName
	$netBios = $null
	$domainDN = $null
	$warning = $false
	try{
		$info = getDomainInfo $LDAPConnection
		if($info.count -eq 1){
			$domainDN = $info[0]
			$warning = $true
		}else{
			if($info.count -eq 2){
				$domainDN = $info[0]
				$netBios = $info[1]
			}else{
				$warning = $true				
			}
		}
	}catch{
		$warning = $true
		if($error.Count -gt 0){
			Write-Log -Message $error[$error.Count-1] -Level Error
		}
	}finally{
		if($warning -eq $true){
			$msg1 = "Cannot retrieve NetBios name of server $($Domain) using LDAP request, provided server name in parameters will be used to name the out file, this name will be used in AD facet during collect to calculate repository code."
			$msg2 = "You must rename manualy out file name to have the format <NetBiosName>.ldif"
			Write-Log -Message $msg1 -Level Warning
			Write-Log -Message $msg2 -Level Warning
			$netBios = $Domain		
		}
	}
	if($false -eq $hrData){
		$current_outputFile = Join-Path $outputDirectory ($netBios.ToUpper() + ".ldif")
		$file = New-Item $current_outputFile -type file -force
		Write-Log -Message "$($file.FullName) file created" -Level Debug
		$Script:outFiles +=  ($netBios.ToUpper() + ".ldif")	
	}else{
		$current_outputFile = Join-Path $outputDirectory ($netBios.ToUpper() + ".csv")
		$file = New-Item $current_outputFile -type file -force
		Write-Log -Message "$($file.FullName) file created" -Level Debug
		$Script:outFiles +=  ($netBios.ToUpper() + ".csv")	

		$dataObject = New-Object -TypeName psobject	
		foreach ($hrAttributeName in $script:hrAttributesOrder){
			$dataObject | Add-Member -MemberType NoteProperty -Name $hrAttributeName -Value ""
		}
		$data =  ConvertTo-Csv -InputObject $dataObject -Delimiter ';' -NoTypeInformation
		Set-Content $data[0] -Path $file.FullName
	}
	# Prepare the request
	$request = New-Object System.directoryServices.Protocols.SearchRequest
	if($ouFilter){
		$request.DistinguishedName= $ouFilter # OU filter
	}else{
		$request.DistinguishedName= $domainDN # Root
	}
	
	if($false -eq $hrData){
		$request.Filter = $filter
	}else{
		if($null -ne $filter -and "" -ne $filter -and "(|(objectcategory=Person)(objectclass=group))" -ne $filter){
			$filter = '(&(objectcategory=Person)(objectClass=user)' + '(' + $filter + '))'
		}else{
			$filter = '(&(objectcategory=Person)(objectClass=user))'
		}		
		$request.Filter = $filter
	}
	$request.Scope = "Subtree"
	[System.DirectoryServices.Protocols.PageResultRequestControl]$requestcontrol = New-Object System.DirectoryServices.Protocols.PageResultRequestControl( 500 ) # Page size
	$request.Controls.Add($requestcontrol) | Out-Null
	$request.TimeLimit=(new-object System.Timespan(0,5,0))

	[void]$request.Attributes.AddRange($Script:attributesList)
	
	# Get the responses page by page
	$pagenb=0
	$stringBuilder = New-Object -TypeName "System.Text.StringBuilder"
	while ($true){	
		$response = $LdapConnection.SendRequest($request,(new-object System.Timespan(0,5,0)) ) -as [System.DirectoryServices.Protocols.SearchResponse];	
		# Dump session options
		if ($pagenb -eq 0 ){
			Write-Log -Message ("Auth Type " +  $LDAPConnection.AuthType) -Level Debug
			Write-Log -Message ("Auto Bind " +  $LDAPConnection.AutoBind) -Level Debug
			$PropertyList=($LDAPConnection.SessionOptions | Get-Member)
			foreach ( $property in $PropertyList ) {
				if ($property.MemberType -eq "Property"){
					$sessionOptions = "SessionOptions " + $property.Name + " : " + $LDAPConnection.SessionOptions.($property.Name)
					Write-Log -Message $sessionOptions -Level Debug
				}
			}
		}
		if ($pagenb -eq 1 ){
			Write-Log -Message ("Second request after $objectCount objects") -Level Debug
		}
		$pagenb=$pagenb+1
	
		[System.DirectoryServices.Protocols.PageResultResponseControl] $responsecontrol=$null;
	
		# Find the pagination token in the response
		if ($response.Controls.Length -gt 0){
			foreach ($ctrl in $response.Controls){
				if ($ctrl -is [System.DirectoryServices.Protocols.PageResultResponseControl]){
					$responsecontrol = $ctrl;
					break;
				}
			}
		}
	
		# No pagination result control ? failed
		if($null -eq $responsecontrol) {
			Write-Log -Message ("No pagination token found") -Level Warning	
			return
		}
	
		# Parse results
		foreach ($foundItem in $response.Entries){		
			# Count and log
			$Script:objectCountPerServer ++
			$objectCount ++
			if ( $objectCount % 1000 -eq 0 ){
				Write-Log -Message ("AD objects - $objectCount objects read ...") -Level Info				
			}
			$groupMembersLimit=$false
			if ($false -eq $hrData) {
				$stringBuilder = $stringBuilder.Append("dn: ").AppendLine( $foundItem.DistinguishedName)
				foreach ($attributeName in $Script:attributesList){
					$attribute = $foundItem.Attributes.Item($attributeName);
					if($attribute){
						handleAttribute $attribute $attributeName
					}
				}
	
				foreach ($name in $foundItem.Attributes.AttributeNames){
					if($name.Contains("member;range=") ){
						$members = $foundItem.Attributes[$name]
						$msg = 'Group {0} have more then {1} members' -f $foundItem.DistinguishedName , $members.Count
						Write-Log -Message $msg -Level Warning				
						Write-Log -Message "Paginating on group members" -Level Debug				
						#paginate on group members
						handleGroupMembers  $foundItem.DistinguishedName $LDAPConnection					
						break
					}
				}
				$stringBuilder = $stringBuilder.AppendLine()
			}else{
				handleAttributesCsv $foundItem
			}
		}
		# write buffer to file
		#Add-Content $current_outputFile $temp_fields			
		addFileContent $current_outputFile $stringBuilder
		$stringBuilder = $stringBuilder.Clear()
	
		# Empty pagination result control ? finished
		if ($responsecontrol.Cookie.Length -eq 0) {
			break;
		}
    
		# Pass the coockie token
		$requestcontrol.Cookie = $responsecontrol.Cookie;
	
	}
}

function handleAttributesCsv{
    Param 
    (   
		$foundItem		
    )	
	
	$dataObject = New-Object -TypeName psobject	
	$index = 1
	$ignore = $false;
	foreach ($hrAttributeName in $script:hrAttributesOrder){
		$attributeName = $script:hrAttributes[$hrAttributeName]
		$value = ""
		if($attributeName){
			$attribute = $foundItem.Attributes.Item($attributeName);
			if($attribute){
				$value = handleAttributeCsv $attribute
			}	
		}
		if(("displayname" -eq $attributeName -or "givenname" -eq $attributeName -or "sn" -eq $attributeName)-and ($null -eq $value -or $value -eq "")){
			$Script:objectCountPerServer --
			$objectCount --

			$ignore = $true
			break
		}
		if("createtimestamp" -eq $attributeName){
			$value = [datetime]::parseexact($value.ToString(), 'yyyyMMddHHmmss.0Z', $null).ToString('dd/MM/yyyy')
		}

		if("accountexpires" -eq $attributeName){
			if(0 -eq $value -or 9223372036854775807 -eq $value){
				$value = ""
			}else{
				$value = [datetime]::FromFileTime($value).ToString('dd/MM/yyyy')
			}
		}

		if("active" -eq $hrAttributeName){
			if(($value -band  0x00000002) -eq  0x00000002){
				$value = "false"
			}else{
				$value = "true"
			}
		}

		$hrAttributeName = $index.ToString() + "_" + $hrAttributeName
		$dataObject | Add-Member -MemberType NoteProperty -Name $hrAttributeName -Value $value
		$index++
		
	}
	if($false -eq $ignore){
		$data =  ConvertTo-Csv -InputObject $dataObject -Delimiter ';' -NoTypeInformation
		$stringBuilder = $stringBuilder.Append($data[1]).AppendLine()	
		# write buffer to file
	}
}

function handleAttributeCsv ([System.DirectoryServices.Protocols.DirectoryAttribute]$attribute){
	$values = @()
	if($attribute.Count -gt 0){		
		For ($i=0; $i -lt $attribute.Count; $i++) {
			$value = $attribute[$i]
			if($value.GetType().Tostring() -eq "System.Byte[]"){
				$value = [System.Convert]::ToBase64String($value)
			}
			$values += $value
		}
	}
	$strValue = $values -join ','
	return $strValue
}

# dumpScriptParameters
# return string containig script parameters 
function dumpScriptParameters(){
    $varList= New-Object -TypeName "System.Text.StringBuilder";
    $varList = $varList.Append('{')
    $varList = $varList.Append("filter").Append('=').Append($filter).Append(';')
    $varList = $varList.Append("attributesfile").Append('=').Append($attributesfile).Append(';')
    $varList = $varList.Append("servers").Append('=').Append($servers).Append(';')
	$varList = $varList.Append("credential").Append('=').Append($credential).Append(';')
    $varList = $varList.Append("logLevel").Append('=').Append($logLevel).Append(';')
	$varList = $varList.Append("errAction").Append('=').Append($errAction)
    $varList = $varList.Append("printLog").Append('=').Append($printLog).Append(';')
    $varList = $varList.Append("memberlimit").Append('=').Append($memberlimit).Append(';')
    $varList = $varList.Append("authType").Append('=').Append($authType).Append(';')
    $varList = $varList.Append("useSSL").Append('=').Append($useSSL).Append(';')
    $varList = $varList.Append("outputDirectory").Append('=').Append($outputDirectory).Append(';')
	$varList = $varList.Append("logDirectory").Append('=').Append($logDirectory).Append(';')
	$varList = $varList.Append("hrdata").Append('=').Append($hrdata).Append(';')
	$varList = $varList.Append("port").Append('=').Append($port).Append(';')
    $varList = $varList.Append('}')
    return $varList = $varList.ToString()
}


Function extractTerminate(){
	Write-Log -Message "Total objects exported is: $($Script:totalObjectCount)" -Level Info

	#Send ldif files to igrc
	if($connectorMode){
		try{
			foreach ($outFile in $Script:outFiles){ send_file $outFile }
		}catch{
			Write-Log -Message "Error occurred when trying to send Ldif files" -Level Error
		}
		foreach ($outFile in $Script:outFiles){ Remove-Item  $outFile }
	}

    # Dump errors in log file if found
	if($error.count -gt 0){
		if($error.count -eq 0){
			Write-Log -Message  "Terminating with $($error.count) error" -Level Warning
		}else{
			Write-Log -Message  "Terminating with $($error.count) errors" -Level Warning
		}

		foreach($err in $error){
			Write-Log -Message $err -Level Error
		}		
		$Script:extractADExitCode = 1
		
		if($connectorMode){
			foreach ($outFile in $Script:outFiles){ send_signal "uncomplete" $outFile }
			#error found data may not be completed, generated files are not approved
		}
	}

    #End datetime
	$datfin = Get-Date
	Write-Log -Message  "End script execution at $($datfin.ToString())" -Level Info
	Write-Log -Message "Total Execution Time:  $($stopwatch.Elapsed)" -Level Info
    Write-Log -Message "Exit with code: $Script:extractADExitCode" -Level Info
	#Send log file to igrc in connector mode
	if($connectorMode){
		$send = $false
		try{
			send_log
			$send = $true
		}catch{
			Write-Log -Message "Error occurred when trying to send log file" -Level Error
		}finally{
			if($send -eq $true){
				Remove-Item $__logfile__
			}
		}
	}
	exit $Script:extractADExitCode
}


Function extractActiveDirectory(){
	# override default attribures if -attributesfile parameter is set
	overrideAttribures
	$attributesListLog = "Attributes to export are: $($Script:attributesList)"
	Write-Log -Message $attributesListLog -Level Debug
	# get domains list from input parameter file
	$domains = @()
	$domains = getServerList
	$domainsLog =  "Servers to export are : $($domains)"
	Write-Log -Message $domainsLog -Level Debug

	$exportCredential = $null
	if($credential){
		if(Test-Path $credential){
			$exportCredential = Import-CliXml $credential                        
		}
	}
			
	foreach ($objDomain in $domains){	
		if($objDomain -ne $null) {
			# handle signle domain
			Write-Log -Message "Iterating on server $($objDomain)" -Level Info
			handleDomain $objDomain
			Write-Log -Message "End Ierating on server $($objDomain): $($Script:objectCountPerServer) Objects exported" -Level Info
			$Script:totalObjectCount += $objectCountPerServer
			$Script:objectCountPerServer = 0
		}
	}
}

function getHrAttributesOrdred() {
	$hrAttributes = ("hrcode","givenname","surname","altname","fullname","titlecode","mail","phone","mobile","internal","employeetype","arrivaldate","departuredate","active",
					"jobtitlecode","jobtitledisplayname","organisationcode","organisationshortname","organisationdisplayname","organisationtype","parentorganisationcode",
					"linemanager","managedorgcode","analyticsgroup")
	return $hrAttributes
}
function getHrAttributes() {
	$hrAttributes = @{}
	$hrAttributes.Add("hrcode","samaccountname")
	$hrAttributes.Add("givenname","givenname")
	$hrAttributes.Add("surname","sn")
	$hrAttributes.Add("altname","")
	$hrAttributes.Add("fullname","displayname")
	$hrAttributes.Add("titlecode","")
	$hrAttributes.Add("mail","mail")
	$hrAttributes.Add("phone","")
	$hrAttributes.Add("mobile","")
	$hrAttributes.Add("internal","")
	$hrAttributes.Add("employeetype","employeetype")
	$hrAttributes.Add("arrivaldate","createtimestamp")
	$hrAttributes.Add("departuredate","accountexpires")
	$hrAttributes.Add("active","useraccountcontrol")
	$hrAttributes.Add("jobtitlecode","title")
	$hrAttributes.Add("jobtitledisplayname","title")
	$hrAttributes.Add("organisationcode","department")
	$hrAttributes.Add("organisationshortname","department")
	$hrAttributes.Add("organisationdisplayname","department")
	$hrAttributes.Add("organisationtype","")
	$hrAttributes.Add("parentorganisationcode","")
	$hrAttributes.Add("linemanager","manager")
	$hrAttributes.Add("managedorgcode","")
	$hrAttributes.Add("analyticsgroup","")
	return $hrAttributes
}

<#
Checks if the input byte array contains only safe values, that is,
the data does not need to be encoded for use with LDIF.
The rules for checking safety are based on the rules for LDIF
(Ldap Data Interchange Format) per RFC 2849.  The data does
not need to be encoded if all the following are true:

The data cannot start with the following byte values:

00 (NUL) 0x00
10 (LF) 0x0A
13 (CR) 0x0D
32 (SPACE) 0x20
58 (:) 0x3A
60 (LESSTHAN) 0x3C
Any character with value greater than 127 (Negative for a byte value)

The data cannot contain any of the following byte values:

00 (NUL) 0x00
10 (LF) 0x0A
13 (CR) 0x0D
Any character with value greater than 127
(Negative for a byte value)

The data cannot end with a space.
#>
function isLDIFSafe {
	
	param (
		$attributeValue
	)

	$enc = [system.Text.Encoding]::UTF8
	[Byte[]]$data = $enc.GetBytes($attributeValue)
	$length = $data.Length
	if($length -gt 0){
		$firstChar = $data[0]
        # unsafe if first character is a NON-SAFE-INIT-CHAR
        if (($firstChar -eq 0x00) -or ($firstChar -eq 0x0A) -or ($firstChar -eq 0x0D) -or ($firstChar -eq 0x20) -or ($firstChar -eq 0x3A) -or ($firstChar -eq 0x3C) -or ($firstChar -lt 0) -or ($firstChar -gt 127)) {
          # non ascii (>127 is negative)
          return $false;
        }
        # unsafe if last character is a space
        if ($data[$length - 1] -eq 0x20) {
          return $false;
        }
        # unsafe if contains any non safe character
        if ($length -gt 1) {
          for ($i = 1; $i -lt $data.Length; $i++) {
            $char = $data[$i]
            if (($char -eq 0x00) -or ($char -eq 0x0A) -or ($char -eq 0x0D) -or ($char -lt 0)) {
              # non ascii (>127 is negative)
              return $false
            }
          }
        }
	}
	return $true
}

#############################################################################################################################################
########################################################### Main Script #####################################################################
#############################################################################################################################################

# Script starting time
$datdebut = Get-Date
$stopwatch = [System.Diagnostics.Stopwatch]::StartNew()

# Script return code 
[int] $Script:extractADExitCode = 0

#Connector counters
$script:connector_counter = 0
$script:connector_p = 0

#Calculate execution directory
$scriptDir = (Get-Location).Path

# Get execution mode if manual or over connector
$connectorVarSet = Get-Variable Connector -Scope Global -ErrorAction SilentlyContinue
$connectorMode = $false
if($connectorVarSet){
	$connectorMode = $true
}	

# Clear errors befor starting script
$error.Clear()

# Calculate log file name
$current_time = $datdebut.Year.ToString()+$datdebut.Month.ToString()+$datdebut.Day.ToString()+$datdebut.Hour.ToString()+$datdebut.Minute.ToString()+$datdebut.Second.ToString()+$datdebut.Millisecond.ToString()	
$__logfile__ = "ExtractAD_" + $current_time + ".log"
if(!$connectorMode){
	if($logDirectory){
		$dir = Get-Item -Path $logDirectory
		$logDirectory = $dir.FullName
		$__logfile__ = Join-Path $logDirectory $__logfile__
	}else{
		$__logfile__ = Join-Path $scriptDir $__logfile__
	}
}
############################ Read parameters in connector mode ##############################
if ($connectorMode -eq $true) {
		

	if($Connector.Options.Options.ContainsKey("filter") -and $Connector.Options.Options.Get_Item("filter") -ne $null) {
		$filter=$Connector.Options.Options.Get_Item("filter") 
	}

	if($Connector.Options.Options.ContainsKey("serverList") -and $Connector.Options.Options.Get_Item("serverList") -ne $null) {
		$servers =$Connector.Options.Options.Get_Item("serverList")
	}
		
	if($Connector.Options.Options.ContainsKey("attributesfile") -and $Connector.Options.Options.Get_Item("attributesfile") -ne $null) {
		$attributesfile =$Connector.Options.Options.Get_Item("attributesfile")
	}
		
	if($Connector.Options.Options.ContainsKey("credential") -and $Connector.Options.Options.Get_Item("credential") -ne $null) {
		$credential = $Connector.Options.Options.Get_Item("credential") 
	}
		
	if($Connector.Options.Options.ContainsKey("port") -and $Connector.Options.Options.Get_Item("port") -ne $null) {
		$port = $Connector.Options.Options.Get_Item("port") 
		if(!$port -or $port -eq 0){
			$port = 389
		}
	}

	if($Connector.Options.Options.ContainsKey("authType") -and $Connector.Options.Options.Get_Item("authType") -ne $null) {
		$authType = $Connector.Options.Options.Get_Item("authType") 
	}

	if($Connector.Options.Options.ContainsKey("useSSL") -and $Connector.Options.Options.Get_Item("useSSL") -ne $null) {
		$useSSL = $Connector.Options.Options.Get_Item("useSSL")
	}

	if($Connector.Options.Options.ContainsKey("logLevel") -and $Connector.Options.Options.Get_Item("logLevel")) {
		$logLevel = $Connector.Options.Options.Get_Item("logLevel") 
	}

	if($Connector.Options.Options.ContainsKey("errAction") -and $Connector.Options.Options.Get_Item("errAction")) {
		$errAction = $Connector.Options.Options.Get_Item("errAction") 
	}
	
	if($Connector.Options.Options.ContainsKey("printLog") -and $Connector.Options.Options.Get_Item("printLog")) {
		$printLog = $Connector.Options.Options.Get_Item("printLog") 
	}else{
		$printLog = $false
	}
	
	if($Connector.Options.Options.ContainsKey("hrData") -and $Connector.Options.Options.Get_Item("hrData")) {
		$hrData = $Connector.Options.Options.Get_Item("hrData") 
	}

	if($Connector.Options.Options.ContainsKey("customHrAttributes") -and $Connector.Options.Options.Get_Item("customHrAttributes")) {
		$customHrAttributes = $Connector.Options.Options.Get_Item("customHrAttributes") 
	}

	if($Connector.Options.Options.ContainsKey("ouFilter") -and $Connector.Options.Options.Get_Item("ouFilter")) {
		$ouFilter = $Connector.Options.Options.Get_Item("ouFilter") 
	}

	$outputDirectory = $scriptDir
	$logDirectory = $scriptDir
}

Write-Log "Starting extractAD at $($datdebut)" -Level Info
Write-Log -Message "Log file in: $($__logfile__)" -Level Info

if($connectorMode -eq $true){
	$msg = "Connector options: $($Connector.Options.Options)"
	Write-Log -Message $msg -Level Debug 
}

$executionMode = "Executing script in connector mode: {0}" -f $connectorMode
Write-Log -Message $executionMode -Level Debug 

# Set script ErrorActionPreference
$ErrorActionPreference = $errAction

# Dump script params if logLevel is debug 
$params = dumpScriptParameters

$scriptParamLog = "Executing script with parameters: $($params)"
Write-Log -Message $scriptParamLog -Level Debug

#calculate output directory Path
if(!$connectorMode){
	if($outputDirectory){
		$dir = Get-Item -Path $outputDirectory
		$outputDirectory = $dir.FullName
	}else{
		$outputDirectory = $scriptDir
	}
}

Add-Type -AssemblyName "System.DirectoryServices"
Add-Type -AssemblyName "System.DirectoryServices.Protocols"

$Script:objectCountPerServer = 0
$Script:totalObjectCount = 0
$Script:outFiles = @()

# attributes list to include in output
$script:attributesList=@("objectcategory","objectguid","objectsid","sidhistory","member","objectclass","accountexpires",
						"badpwdcount","displayname","givenname","lastlogon","logoncount","lastlogontimestamp","mail","manager",
						"pwdlastset","samaccountname","sn","whencreated","useraccountcontrol","createtimestamp","description",
						"grouptype","managedby","modifytimestamp","legacyexchangedn","whenCreated","PrimaryGroupID")

#mapping between rh attributes an AD attribues
if($true -eq $hrData){
	$script:attributesList += "department"
	$script:attributesList += "title"
	$script:attributesList += "employeetype"
	$script:hrAttributesOrder = @()
	$script:hrAttributesOrder = getHrAttributesOrdred
	
	$script:hrAttributes = @{}
	$script:hrAttributes = getHrAttributes
	overrideHrAttribures
}

if($errAction -eq "Stop"){	
	#dump exception in finally block
	try{
		extractActiveDirectory
	}finally{ 
		extractTerminate
	}
}else{
	extractActiveDirectory
	extractTerminate
}


# SIG # Begin signature block
# MIImFQYJKoZIhvcNAQcCoIImBjCCJgICAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUjKdyP9Q/39B4ppt8wgrsqcxR
# Uu+gggmhMIIElDCCA3ygAwIBAgIOSBtqBybS6D8mAtSCWs0wDQYJKoZIhvcNAQEL
# BQAwTDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjMxEzARBgNVBAoT
# Ckdsb2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcNMTYwNjE1MDAwMDAw
# WhcNMjQwNjE1MDAwMDAwWjBaMQswCQYDVQQGEwJCRTEZMBcGA1UEChMQR2xvYmFs
# U2lnbiBudi1zYTEwMC4GA1UEAxMnR2xvYmFsU2lnbiBDb2RlU2lnbmluZyBDQSAt
# IFNIQTI1NiAtIEczMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjYVV
# I6kfU6/J7TbCKbVu2PlC9SGLh/BDoS/AP5fjGEfUlk6Iq8Zj6bZJFYXx2Zt7G/3Y
# SsxtToZAF817ukcotdYUQAyG7h5LM/MsVe4hjNq2wf6wTjquUZ+lFOMQ5pPK+vld
# sZCH7/g1LfyiXCbuexWLH9nDoZc1QbMw/XITrZGXOs5ynQYKdTwfmOPLGC+MnwhK
# kQrZ2TXZg5J2Yl7fg67k1gFOzPM8cGFYNx8U42qgr2v02dJsLBkwXaBvUt/RnMng
# Ddl1EWWW2UO0p5A5rkccVMuxlW4l3o7xEhzw127nFE2zGmXWhEpX7gSvYjjFEJtD
# jlK4PrauniyX/4507wIDAQABo4IBZDCCAWAwDgYDVR0PAQH/BAQDAgEGMB0GA1Ud
# JQQWMBQGCCsGAQUFBwMDBggrBgEFBQcDCTASBgNVHRMBAf8ECDAGAQH/AgEAMB0G
# A1UdDgQWBBQPOueslJF0LZYCc4OtnC5JPxmqVDAfBgNVHSMEGDAWgBSP8Et/qC5F
# JK5NUPpjmove4t0bvDA+BggrBgEFBQcBAQQyMDAwLgYIKwYBBQUHMAGGImh0dHA6
# Ly9vY3NwMi5nbG9iYWxzaWduLmNvbS9yb290cjMwNgYDVR0fBC8wLTAroCmgJ4Yl
# aHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9yb290LXIzLmNybDBjBgNVHSAEXDBa
# MAsGCSsGAQQBoDIBMjAIBgZngQwBBAEwQQYJKwYBBAGgMgFfMDQwMgYIKwYBBQUH
# AgEWJmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMA0GCSqG
# SIb3DQEBCwUAA4IBAQAVhCgM7aHDGYLbYydB18xjfda8zzabz9JdTAKLWBoWCHqx
# mJl/2DOKXJ5iCprqkMLFYwQL6IdYBgAHglnDqJQy2eAUTaDVI+DH3brwaeJKRWUt
# TUmQeGYyDrBowLCIsI7tXAb4XBBIPyNzujtThFKAzfCzFcgRCosFeEZZCNS+t/9L
# 9ZxqTJx2ohGFRYzUN+5Q3eEzNKmhHzoL8VZEim+zM9CxjtEMYAfuMsLwJG+/r/uB
# AXZnxKPo4KvcM1Uo42dHPOtqpN+U6fSmwIHRUphRptYCtzzqSu/QumXSN4NTS35n
# fIxA9gccsK8EBtz4bEaIcpzrTp3DsLlUo7lOl8oUMIIFBTCCA+2gAwIBAgIMVk48
# nRXJHnELbPAoMA0GCSqGSIb3DQEBCwUAMFoxCzAJBgNVBAYTAkJFMRkwFwYDVQQK
# ExBHbG9iYWxTaWduIG52LXNhMTAwLgYDVQQDEydHbG9iYWxTaWduIENvZGVTaWdu
# aW5nIENBIC0gU0hBMjU2IC0gRzMwHhcNMTgwNDE2MTQ1NDAyWhcNMjEwNjAzMDkw
# NjM0WjB/MQswCQYDVQQGEwJGUjEbMBkGA1UEBxMSQVNOSUVSRVMgU1VSIFNFSU5F
# MRYwFAYDVQQKEw1CUkFJTldBVkUgU0FTMRYwFAYDVQQDEw1CUkFJTldBVkUgU0FT
# MSMwIQYJKoZIhvcNAQkBFhRzdXBwb3J0QGJyYWlud2F2ZS5mcjCCASIwDQYJKoZI
# hvcNAQEBBQADggEPADCCAQoCggEBALeLRKNaEEd9gprN7Pcw4EzHQKl16qw77ZQW
# jGqqudKgg1Oxq/+GWUXPGEe3kqkxC2uFqr9rAKx+7SfRIELdhxserQSKdOqzmNom
# PTqpBxSDuRveq868f+GkBPntSUxgOaQn0+/r0f4gfJ/9w+bwLtqwBIbBsNPe9kKO
# 37ZXDtB/5ZigoO5MGGWfSBjEJJWztVWM8Y3SmaqVH8Ur6o6DG4HO6qTJlfqhhskg
# CD+B3SLRrqyugY1/OBIQ0nKJ37tl1TWvI0AcVKtsa0PwIU7TqHF/NovoCl7Q5KtS
# /zFu6EVIS72qy7t4M7cC4vRoZo5ydipyHRHQJeswqaOzfZAjYx0CAwEAAaOCAaQw
# ggGgMA4GA1UdDwEB/wQEAwIHgDCBlAYIKwYBBQUHAQEEgYcwgYQwSAYIKwYBBQUH
# MAKGPGh0dHA6Ly9zZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzY29kZXNp
# Z25zaGEyZzNvY3NwLmNydDA4BggrBgEFBQcwAYYsaHR0cDovL29jc3AyLmdsb2Jh
# bHNpZ24uY29tL2dzY29kZXNpZ25zaGEyZzMwVgYDVR0gBE8wTTBBBgkrBgEEAaAy
# ATIwNDAyBggrBgEFBQcCARYmaHR0cHM6Ly93d3cuZ2xvYmFsc2lnbi5jb20vcmVw
# b3NpdG9yeS8wCAYGZ4EMAQQBMAkGA1UdEwQCMAAwPwYDVR0fBDgwNjA0oDKgMIYu
# aHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9nc2NvZGVzaWduc2hhMmczLmNybDAT
# BgNVHSUEDDAKBggrBgEFBQcDAzAdBgNVHQ4EFgQUSwIIUUxAfw3iBq0rXrAh+/fg
# pgwwHwYDVR0jBBgwFoAUDzrnrJSRdC2WAnODrZwuST8ZqlQwDQYJKoZIhvcNAQEL
# BQADggEBAAGnPe4ziy32SZL1bQcx8LtOnI8Iz1MpHqIGn6n3X7f9uTyGwN3nzrYd
# xVtWpFpqOa6kKQSQ0qvHY0TxOFHVuktXt0+jeu0wQEq/X36sLnwJyUM2KlRP0DXe
# UDBU9Xqeybc2bMVDJFKsa5RwsnGSaKxTBnp10STAtaUNpYwdQ2tcQMWCTObpRCre
# SXM87EECS/F7E5hwx1pFc8yoEgTLMI+z3krQrBDHEM2gWnPgy88WL91GxKtLC2H5
# VVv10oB1vYL7q2Kp+Mew5RGXOQgdGfp/hnbPLwJlLbMEVGFimGG1BQIFjIfSDakt
# C+9oj3H1B2z90KGkN/BLvzkADFx9FnUxghveMIIb2gIBATBqMFoxCzAJBgNVBAYT
# AkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTAwLgYDVQQDEydHbG9iYWxT
# aWduIENvZGVTaWduaW5nIENBIC0gU0hBMjU2IC0gRzMCDFZOPJ0VyR5xC2zwKDAJ
# BgUrDgMCGgUAoHAwEAYKKwYBBAGCNwIBDDECMAAwGQYJKoZIhvcNAQkDMQwGCisG
# AQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEOMAwGCisGAQQBgjcCARUwIwYJKoZIhvcN
# AQkEMRYEFA0YU5Bhv6+R0iMo6y4Q50ArJe2FMA0GCSqGSIb3DQEBAQUABIIBAHA6
# LEGWCT3WUliTRemahRVAtLXOplNpnAIToUgS5yN3CVyVpYesjGEEa+wvSSqj4EUq
# n2tFfh0B4GCWNtjzUEnWLpWfHTuG1FL74aUbA0WoEBkhkbyTQNLICbMOwTlteNxF
# 1P6EIutHKLvM8pduGR9PtmfMNK6JPMJdnFQZYCt28a+C6eyrkLSSMwxlXuYIhW7c
# lLf+dpFSEJFUJvnBF7sUjrCC2me8pFe9nfmvuqYKYE153cKchTSeqjFiapRJ4kVG
# YUVQEkuWj0LQqSvGoQwb5HOcA2bxiiB5jYXkAwyCPqoU1l/Uq0S8rxMBBpbDuMj/
# Y79qsR454l3ZS3eSn0ChghnXMIIZ0wYKKwYBBAGCNwMDATGCGcMwghm/BgkqhkiG
# 9w0BBwKgghmwMIIZrAIBAzEPMA0GCWCGSAFlAwQCAQUAMIHcBgsqhkiG9w0BCRAB
# BKCBzASByTCBxgIBAQYJKwYBBAGgMgIDMDEwDQYJYIZIAWUDBAIBBQAEIFEkxTZT
# /VO7VaLatgtfdpBaeXXXopGplpJJ63USklW9AhR6tfP4/QWWg13T6UHGRiuERNzq
# tRgPMjAyMTAzMzAxMjMyMzFaMAMCAQGgV6RVMFMxCzAJBgNVBAYTAkJFMRkwFwYD
# VQQKDBBHbG9iYWxTaWduIG52LXNhMSkwJwYDVQQDDCBHbG9iYWxzaWduIFRTQSBm
# b3IgQWR2YW5jZWQgLSBHNKCCFWQwggZVMIIEPaADAgECAhABNQHqF8U5KY2+WR5S
# i/ZgMA0GCSqGSIb3DQEBCwUAMFsxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9i
# YWxTaWduIG52LXNhMTEwLwYDVQQDEyhHbG9iYWxTaWduIFRpbWVzdGFtcGluZyBD
# QSAtIFNIQTM4NCAtIEc0MB4XDTIxMDIwMjE1MzUzM1oXDTMyMDMwNjE1MzUzM1ow
# UzELMAkGA1UEBhMCQkUxGTAXBgNVBAoMEEdsb2JhbFNpZ24gbnYtc2ExKTAnBgNV
# BAMMIEdsb2JhbHNpZ24gVFNBIGZvciBBZHZhbmNlZCAtIEc0MIIBojANBgkqhkiG
# 9w0BAQEFAAOCAY8AMIIBigKCAYEAjqEEkM+3YLc+JNUnzJiGYjjIecuGrPzSr/kB
# LDCxX/oPvloYgay2OXkDSmqeSCmBkBiaP38DHf52/83CRBH96omHlq1s9G9Aqp/e
# ya3UWsCypebJTXHk3+fY9Mua+cAT/uTUBkNED4X//jo2ojq0axjLC0D6UIOE2lZl
# oazhn26LZDteC73IpXee0o1fgjlXGAmCFPVYUmthfihIX+l49hdE7NdLMd25yISO
# PeIxwAe4MTGUkENX4us+HsG7WOYiqSlKUmpNgAdw4XOG36zBBJ6mv7NfJgXftfJz
# bJFC3TdLVghZeTSzqF9PvAr9pjAm0CP1mkTLBCzNryi8T167/aynMBzDdC7VukV/
# 7AuauOgnKfHe0OCdAASZbFpSYhXGiisQP1U8X205mhYXl94k3HCm0TSwXUHt+yJB
# 0O5/UekNkPqa7dK9QfdAwAPMcCQnF92S0shrlCENcXJQll2uuKCznO3t2D5UKyVb
# /OtOvTnLP3gg7K/9jJmGhfjo65xxAgMBAAGjggGbMIIBlzAOBgNVHQ8BAf8EBAMC
# B4AwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwgwHQYDVR0OBBYEFGaZ/XwVngKWd3w6
# IbBBqtLaT9D5MEwGA1UdIARFMEMwQQYJKwYBBAGgMgEeMDQwMgYIKwYBBQUHAgEW
# Jmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMAkGA1UdEwQC
# MAAwgZAGCCsGAQUFBwEBBIGDMIGAMDkGCCsGAQUFBzABhi1odHRwOi8vb2NzcC5n
# bG9iYWxzaWduLmNvbS9jYS9nc3RzYWNhc2hhMzg0ZzQwQwYIKwYBBQUHMAKGN2h0
# dHA6Ly9zZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzdHNhY2FzaGEzODRn
# NC5jcnQwHwYDVR0jBBgwFoAU6hbGaefjy1dFOTOk8EC+0MO9ZZYwQQYDVR0fBDow
# ODA2oDSgMoYwaHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9jYS9nc3RzYWNhc2hh
# Mzg0ZzQuY3JsMA0GCSqGSIb3DQEBCwUAA4ICAQCjE76xTHcW0fBuYAAf7Iqg8qSq
# in9MP1OFghKZ4qfN27tOgFLq2AdIUrOK8xY4RttMBj/57B7Svja9N8NR835TFgtY
# f0lsCbP5YXjexftGlOzmzru9A0094ZSWBiD6Pn8JZn8W8ddFv/LeKeFejYytTc2h
# nDU34J/+mcmhbiO/ybBpU0MWd8SSYxKHGbha3DD6SwzqjRY3G0eUoTEVJWMEqHI0
# cW3Jmysisjvs+fR7u46MGg/htjgDlgc9uTyK0P2OEXAM3jYZZxsr7d18H11o2jvd
# a7ygY1EecV+REZmosu01mGJdZi+V8rNaCtwwvqauzx2KiIeooqgnFTRcWifPDRXO
# 5m5mBRzCGAEGG1nxhizl6JVIkZsZLawCGko2FNx5XSxYBrKq2SfxP0NhhCmMsK0X
# Hzvvvd2JgZuaN3K9lISYw+0oTcNb4xiJfH6bWlGsnNzejnpt+ZonaPbxz2Boly9Q
# EiH88l/+5S48oheK6qoOARs9QfMXWEoOuFFYaPR2gbX09EdR3aUkAgDZNKHnxVJ6
# SfPiW0QxvfqIAIF1I2oVuTeFqtXEkJsFcxPgk6EEQiz8um55fjh0jZEMau9cUXEz
# y7vTqykuBN9902CLQXZq6ieOnPDo+OXgZ1Sds6XcNcgpdh2wRc0Anb4E1ua0i7oK
# 6VVfbkkSYDLbnP8pjzCCBlkwggRBoAMCAQICDQHsHJJA3v0uQF18R3QwDQYJKoZI
# hvcNAQEMBQAwTDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjYxEzAR
# BgNVBAoTCkdsb2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcNMTgwNjIw
# MDAwMDAwWhcNMzQxMjEwMDAwMDAwWjBbMQswCQYDVQQGEwJCRTEZMBcGA1UEChMQ
# R2xvYmFsU2lnbiBudi1zYTExMC8GA1UEAxMoR2xvYmFsU2lnbiBUaW1lc3RhbXBp
# bmcgQ0EgLSBTSEEzODQgLSBHNDCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoC
# ggIBAPAC4jAj+uAb4Zp0s691g1+pR1LHYTpjfDkjeW10/DHkdBIZlvrOJ2JbrgeK
# J+5Xo8Q17bM0x6zDDOuAZm3RKErBLLu5cPJyroz3mVpddq6/RKh8QSSOj7rFT/82
# QaunLf14TkOI/pMZF9nuMc+8ijtuasSI8O6X9tzzGKBLmRwOh6cm4YjJoOWZ4p70
# nEw/XVvstu/SZc9FC1Q9sVRTB4uZbrhUmYqoMZI78np9/A5Y34Fq4bBsHmWCKtQh
# x5T+QpY78Quxf39GmA6HPXpl69FWqS69+1g9tYX6U5lNW3TtckuiDYI3GQzQq+pa
# we8P1Zm5P/RPNfGcD9M3E1LZJTTtlu/4Z+oIvo9Jev+QsdT3KRXX+Q1d1odDHnTE
# cCi0gHu9Kpu7hOEOrG8NubX2bVb+ih0JPiQOZybH/LINoJSwspTMe+Zn/qZYstTY
# QRLBVf1ukcW7sUwIS57UQgZvGxjVNupkrs799QXm4mbQDgUhrLERBiMZ5PsFNETq
# CK6dSWcRi4LlrVqGp2b9MwMB3pkl+XFu6ZxdAkxgPM8CjwH9cu6S8acS3kISTeyp
# JuV3AqwOVwwJ0WGeJoj8yLJN22TwRZ+6wT9Uo9h2ApVsao3KIlz2DATjKfpLsBzT
# N3SE2R1mqzRzjx59fF6W1j0ZsJfqjFCRba9Xhn4QNx1rGhTfAgMBAAGjggEpMIIB
# JTAOBgNVHQ8BAf8EBAMCAYYwEgYDVR0TAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQU
# 6hbGaefjy1dFOTOk8EC+0MO9ZZYwHwYDVR0jBBgwFoAUrmwFo5MT4qLn4tcc1sfw
# f8hnU6AwPgYIKwYBBQUHAQEEMjAwMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcDIu
# Z2xvYmFsc2lnbi5jb20vcm9vdHI2MDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly9j
# cmwuZ2xvYmFsc2lnbi5jb20vcm9vdC1yNi5jcmwwRwYDVR0gBEAwPjA8BgRVHSAA
# MDQwMgYIKwYBBQUHAgEWJmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9z
# aXRvcnkvMA0GCSqGSIb3DQEBDAUAA4ICAQB/4ojZV2crQl+BpwkLusS7KBhW1ky/
# 2xsHcMb7CwmtADpgMx85xhZrGUBJJQge5Jv31qQNjx6W8oaiF95Bv0/hvKvN7sAj
# jMaF/ksVJPkYROwfwqSs0LLP7MJWZR29f/begsi3n2HTtUZImJcCZ3oWlUrbYsbQ
# swLMNEhFVd3s6UqfXhTtchBxdnDSD5bz6jdXlJEYr9yNmTgZWMKpoX6ibhUm6rT5
# fyrn50hkaS/SmqFy9vckS3RafXKGNbMCVx+LnPy7rEze+t5TTIP9ErG2SVVPdZ2s
# b0rILmq5yojDEjBOsghzn16h1pnO6X1LlizMFmsYzeRZN4YJLOJF1rLNboJ1pdqN
# HrdbL4guPX3x8pEwBZzOe3ygxayvUQbwEccdMMVRVmDofJU9IuPVCiRTJ5eA+kiJ
# Jyx54jzlmx7jqoSCiT7ASvUh/mIQ7R0w/PbM6kgnfIt1Qn9ry/Ola5UfBFg0Cont
# glDk0Xuoyea+SKorVdmNtyUgDhtRoNRjqoPqbHJhSsn6Q8TGV8Wdtjywi7C5HDHv
# ve8U2BRAbCAdwi3oC8aNbYy2ce1SIf4+9p+fORqurNIveiCx9KyqHeItFJ36lmod
# xjzK89kcv1NNpEdZfJXEQ0H5JeIsEH6B+Q2Up33ytQn12GByQFCVINRDRL76oJXn
# IFm2eMakaqoimzCCBUcwggQvoAMCAQICDQHyQEJAzv0i2+lscfwwDQYJKoZIhvcN
# AQEMBQAwTDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0gUjMxEzARBgNV
# BAoTCkdsb2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcNMTkwMjIwMDAw
# MDAwWhcNMjkwMzE4MTAwMDAwWjBMMSAwHgYDVQQLExdHbG9iYWxTaWduIFJvb3Qg
# Q0EgLSBSNjETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UEAxMKR2xvYmFsU2ln
# bjCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAJUH6HPKZvnsFMp7PPcN
# CPG0RQssgrRIxutbPK6DuEGSMxSkb3/pKszGsIhrxbaJ0cay/xTOURQh7ErdG1rG
# 1ofuTToVBu1kZguSgMpE3nOUTvOniX9PeGMIyBJQbUJmL025eShNUhqKGoC3GYEO
# fsSKvGRMIRxDaNc9PIrFsmbVkJq3MQbFvuJtMgamHvm566qjuL++gmNQ0PAYid/k
# D3n16qIfKtJwLnvnvJO7bVPiSHyMEAc4/2ayd2F+4OqMPKq0pPbzlUoSB239jLKJ
# z9CgYXfIWHSw1CM69106yqLbnQneXUQtkPGBzVeS+n68UARjNN9rkxi+azayOeSs
# JDa38O+2HBNXk7besvjihbdzorg1qkXy4J02oW9UivFyVm4uiMVRQkQVlO6jxTiW
# m05OWgtH8wY2SXcwvHE35absIQh1/OZhFj931dmRl4QKbNQCTXTAFO39OfuD8l4U
# oQSwC+n+7o/hbguyCLNhZglqsQY6ZZZZwPA1/cnaKI0aEYdwgQqomnUdnjqGBQCe
# 24DWJfncBZ4nWUx2OVvq+aWh2IMP0f/fMBH5hc8zSPXKbWQULHpYT9NLCEnFlWQa
# Yw55PfWzjMpYrZxCRXluDocZXFSxZba/jJvcE+kNb7gu3GduyYsRtYQUigAZcIN5
# kZeR1BonvzceMgfYFGM8KEyvAgMBAAGjggEmMIIBIjAOBgNVHQ8BAf8EBAMCAQYw
# DwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUrmwFo5MT4qLn4tcc1sfwf8hnU6Aw
# HwYDVR0jBBgwFoAUj/BLf6guRSSuTVD6Y5qL3uLdG7wwPgYIKwYBBQUHAQEEMjAw
# MC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcDIuZ2xvYmFsc2lnbi5jb20vcm9vdHIz
# MDYGA1UdHwQvMC0wK6ApoCeGJWh0dHA6Ly9jcmwuZ2xvYmFsc2lnbi5jb20vcm9v
# dC1yMy5jcmwwRwYDVR0gBEAwPjA8BgRVHSAAMDQwMgYIKwYBBQUHAgEWJmh0dHBz
# Oi8vd3d3Lmdsb2JhbHNpZ24uY29tL3JlcG9zaXRvcnkvMA0GCSqGSIb3DQEBDAUA
# A4IBAQBJrF7Fg/Nay2EqTZdKFSmf5BSQqgn5xHqfNRiKCjMVbXKHIk5BP20Knhiu
# 2+Jf/JXRLJgUO47B8DZZefONgc909hik5OFoz+9/ZVlC6cpVObzTxSbucTj61yED
# D7dO2VtgakO0fQnQYGHdqu0AXk4yHuCybJ48ssK7mNOQdmpprRrcqInaWE/Swosy
# Ss5U+zjpOwcLdQoR2wt8JSfxrCbPEVPm3MbiYTUy9M7dg+MZOuvCaKNyAMgkPE64
# UzyxF6vmNSz500Ip5l9gA6xCYaaxV2ozQt81MYbKPjcr2sTaJPVOEvK2ubdH6rsg
# rWEWt6Az4y2Jp7yzPAF/IxqACTTpMIIDXzCCAkegAwIBAgILBAAAAAABIVhTCKIw
# DQYJKoZIhvcNAQELBQAwTDEgMB4GA1UECxMXR2xvYmFsU2lnbiBSb290IENBIC0g
# UjMxEzARBgNVBAoTCkdsb2JhbFNpZ24xEzARBgNVBAMTCkdsb2JhbFNpZ24wHhcN
# MDkwMzE4MTAwMDAwWhcNMjkwMzE4MTAwMDAwWjBMMSAwHgYDVQQLExdHbG9iYWxT
# aWduIFJvb3QgQ0EgLSBSMzETMBEGA1UEChMKR2xvYmFsU2lnbjETMBEGA1UEAxMK
# R2xvYmFsU2lnbjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMwldpB5
# BngiFvXAg7aEyiie/QV2EcWtiHL8RgJDx7KKnQRfJMsuS+FggkbhUqsMgUdwbN1k
# 0ev1LKMPgj0MK66X17YUhhB5uzsTgHeMCOFJ0mpiLx9e+pZo34knlTifBtc+ycsm
# WQ1z3rDI6SYOgxXG71uL0gRgykmmKPZpO/bLyCiR5Z2KYVc3rHQU3HTgOu5yLy6c
# +9C7v/U9AOEGM+iCK65TpjoWc4zdQQ4gOsC0p6Hpsk+QLjJg6VfLuQSSaGjlOCZg
# dbKfd/+RFO+uIEn8rUAVSNECMWEZXriX7613t2Saer9fwRPvm2L7DWzgVGkWqQPa
# bumDk3F2xmmFghcCAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQF
# MAMBAf8wHQYDVR0OBBYEFI/wS3+oLkUkrk1Q+mOai97i3Ru8MA0GCSqGSIb3DQEB
# CwUAA4IBAQBLQNvAUKr+yAzv95ZURUm7lgAJQayzE4aGKAczymvmdLm6AC2upArT
# 9fHxD4q/c2dKg8dEe3jgr25sbwMpjjM5RcOO5LlXbKr8EpbsU8Yt5CRsuZRj+9xT
# aGdWPoO4zzUhw8lo/s7awlOqzJCK6fBdRoyV3XpYKBovHd7NADdBj+1EbddTKJd+
# 82cEHhXXipa0095MJ6RMG3NzdvQXmcIfeg7jLQitChws/zyrVQ4PkX4268NXSb7h
# Li18YIvDQVETI53O9zJrlAGomecsMx86OyXShkDOOyyGeMlhLxS67ttVb9+E7gUJ
# Tb0o2HLO02JQZR7rkpeDMdmztcpHWD9fMYIDTTCCA0kCAQEwbzBbMQswCQYDVQQG
# EwJCRTEZMBcGA1UEChMQR2xvYmFsU2lnbiBudi1zYTExMC8GA1UEAxMoR2xvYmFs
# U2lnbiBUaW1lc3RhbXBpbmcgQ0EgLSBTSEEzODQgLSBHNAIQATUB6hfFOSmNvlke
# Uov2YDANBglghkgBZQMEAgEFAKCCAS8wGgYJKoZIhvcNAQkDMQ0GCyqGSIb3DQEJ
# EAEEMC0GCSqGSIb3DQEJNDEgMB4wDQYJYIZIAWUDBAIBBQChDQYJKoZIhvcNAQEL
# BQAwLwYJKoZIhvcNAQkEMSIEIJGDvU1LFXOGKiG4OWisQUdMwgOlB+gCoJueCHos
# DxodMIGwBgsqhkiG9w0BCRACLzGBoDCBnTCBmjCBlwQgJFrLN3sm1qfhXRKmsHmQ
# bebHHnMY+Yb6Ds1LQCWNt7MwczBfpF0wWzELMAkGA1UEBhMCQkUxGTAXBgNVBAoT
# EEdsb2JhbFNpZ24gbnYtc2ExMTAvBgNVBAMTKEdsb2JhbFNpZ24gVGltZXN0YW1w
# aW5nIENBIC0gU0hBMzg0IC0gRzQCEAE1AeoXxTkpjb5ZHlKL9mAwDQYJKoZIhvcN
# AQELBQAEggGAWdgLPJ2Pmhyppa7r690+kzPd/Fu82wv2wkF2y8kiATDl2zEtO7Pt
# 9fv9rK9xbyYd1uYkKTNnwRwIiHLPU2mBDBxYJbT9fH8R0zkfy4tKhoUBePTglvbV
# 8AFTt/oC0D+DIKFowIR2haZ8FaW5hCXH9+DNoSKlLcdQHHEtLhGku5LyrXSDIQYH
# mYkRfpV1FzHjDGAZscdFvDUuVGa/378kqGIl7z6Ljhj5YfAOArw0pwsSd1pRnP/i
# mH2YX8TYCLEizqm8fQrL4ipKkVE0uCoQfLn6Y7gpGUBDnR57iqrPQbl9DFEsAl72
# mHwPmnPvMxbPY1lEuU1rPGUfEa8VzMyIyd85J8/Vnw+B9+I+VkKLW9pdYklDnZ71
# TaGtmUe7UjCz52eyo0yQ2kcDTUjP0ElE2UIGpYjBdmLtSRzeLl6A91IkcKGVs/mm
# yIGdI96XbZa6Qdse0kPyN3PLVFsvGOWuwxnj0wmL5vF9EviQTH4ZPml7ton36XVD
# T8DFOVKg1xKi
# SIG # End signature block
