#Help#####################################################################################################################################
<# 
.SYNOPSIS
COPYRIGHT BRAINWAVE, all rights reserved.
This computer program is protected by copyright law and international treaties.
Unauthorized duplication or distribution of this program, or any portion of it, may result in severe civil or criminal penalties, and will be prosecuted to the maximum extent possible under the law.

extract_Windows_local_accounts_and_groups_remotely Version 2.5 (c) Brainwave 2019
Extract local groups and local user accounts from a Windows system.
 
.DESCRIPTION
	USAGE:
	  ./extract_Windows_local_accounts_and_groups_remotely.ps1 -ExtractPath <outputDirectory> -target_comp <targeted computer name> -Cred y
	  
	GENERATED FILES:
	- The localUsers file that contains the local accounts (CSV),
	- The localGroups file that contains the list of the groups (CSV),
	- The groupMembers file which contains for each group the list of the members (CSV),
	- A .name file which is used during the silo iteration process (not used as a source but compulsory for the collect step),
	- A log file.
	
	SCRIPT PARAMETERS:
	-ExtractPath <outputDirectory>: directory where generated files will be saved (if not specified, files will be registered in the current directory)
	-target_comp <targeted computer name>: computer on which the script has to be launched
	-servers_list_file <servers list .txt file>: text file with the list of servers the script must be performed on
	-Cred: parameter to specify is the script must be run with manually entered credentials (-Cred y to ask for credentials, no parameter call to not ask for credentials) 

.EXAMPLE
	./extract_Windows_local_accounts_and_groups_remotely.ps1 -ExtractPath C:\User\jwayne\files\windowslocalaccountsextraction -target_comp LTDELVOS_008


.EXAMPLE
	./extract_Windows_local_accounts_and_groups_remotely.ps1 -ExtractPath C:\User\jwayne\files\windowslocalaccountsextraction -target_comp LTDELVOS_008 -Cred y


.EXAMPLE
	./extract_Windows_local_accounts_and_groups_remotely.ps1 -ExtractPath C:\User\jwayne\files\windowslocalaccountsextraction -servers_list_file "C:\User\jwayne\files\servers_list.txt"
	

.EXAMPLE
	./extract_Windows_local_accounts_and_groups_remotely.ps1 -ExtractPath C:\User\jwayne\files\windowslocalaccountsextraction -servers_list_file "C:\User\jwayne\files\servers_list.txt" -Cred y

#>


#Parameters#####################################################################################################################################
Param(
# Path to directory where files are saved
	[String]$ExtractPath = "./",
	[String]$target_comp = $env:computername,
	[String]$servers_list_file,
	[String]$Cred
	)


function Collect_accounts ($computername) {
	#Shared variables###############################################################################################################################
	$output_encoding="UTF8"
	$errorFound = $False
	$datehour = Get-Date -Format g

	#Test the given path############################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$test_path = Test-Path -Path $ExtractPath
		If ($test_path -eq $True) {
		}
		Else {
			exit 2
		}
	}
	catch{
	$errorFound = $True
	write-host "WARNING: Wrong path." -ForegroundColor Yellow
	}


	#Log file#######################################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$test_log_path = Test-Path -Path $ExtractPath/$computername".log"
		If ($test_log_path -eq $True) {
			$Logfile = "$ExtractPath/$computername.log"
			Add-content $Logfile -value " "
			Add-content $Logfile -value $datehour
		}
		Else {
			$Logfile = New-Item -ItemType file -Path $ExtractPath/$computername".log"
			Add-content $Logfile -value " "
			Add-content $Logfile -value $datehour
		}

		Function Log
		{
			Param ([string]$logstring)
			Add-content $Logfile -value $logstring
		}
	}
	catch{
		$errorFound = $True
		write-host "WARNING: Problem creating the log file." -ForegroundColor Yellow
		exit 2
	}


	#Timer##########################################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$globalWatch = [system.diagnostics.StopWatch]::startNew();
		Log "INFO: Timer successfully launched."
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem launching Timer."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	#PS Version##########################################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$psversion = ($PSVersionTable.PSVersion);
		Log "INFO: PS version = $psversion"
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem catching PS Version."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	#OS Version##########################################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$osversion = ([System.Environment]::OSVersion.Version);
		Log "INFO: OS version = $osversion"
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem catching OS Version."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	###Test to determine domain/workgroup membership################################################################################################
    try{
        $ErrorActionPreference = 'Stop'
        
		$test_domain = (Get-WmiObject -Class Win32_ComputerSystem).PartOfDomain
		Log "INFO: test_domain variable flag."
		$workgroup = (Get-WmiObject -Class Win32_ComputerSystem).Workgroup
		Log "INFO: workgroup variable flag."
        
        If ($test_domain -eq $True) { 
            $domain = $env:userdomain
            Log "INFO: Computer is linked to a domain."
        }
        Else {
            $domain = $workgroup 
            Log "INFO: Computer is not linked to a domain. It is linked to a group."
        }
    }

	catch{
		$errorFound = $True
		Log "ERROR: Problem finding domain/workgroup computer membership."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}

	#Creation of the .name file#####################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$test_path = Test-Path -Path $ExtractPath/$computername".name"
		If ($test_path -eq $True) {
			Log "INFO: .name file already exists. No creation needed."
		}
		Else {
			New-Item -ItemType file -Path $ExtractPath/$computername".name" | Out-Null
			Log "INFO: .name file successfully created."
		}
	}
	catch{
	$errorFound = $True
	Log "ERROR: .name file creation procedure failed."
	$ErrorMessage = $_.Exception.Message
	Log "	$ErrorMessage"
	}


	###Collection of the local users################################################################################################################
	try{
		$output_file_local_users = New-Item -ItemType file -Path $ExtractPath/$computername"_localUsers.csv" -Force
		Log "INFO: output_file_local_users variable flag."
		Set-Content "UserName;SID;PasswordAge;LastLogin;MinPasswordLength;MinPasswordAge;MaxPasswordAge;BadPasswordAttempts;MaxBadPasswordsAllowed;Description;AuthenticationType;AccountDisable;Lockout;Password_not_required;Password_cant_change;Dont_expire_password;Password_expired" -Encoding $output_encoding -Path $ExtractPath/$computername"_localUsers.csv"
		Log "INFO: local users output file created."
		if ($Cred -ne [string]::empty){
			[System.Management.Automation.PSCredential]$Credential = $(Get-Credential)
			$computer = New-Object System.DirectoryServices.DirectoryEntry("WinNT://$domain/$computername", $Credential.UserName, $Credential.GetNetworkCredential().Password)
			Log "INFO: Credentials parameter."
		}
		else {
			$computer = [ADSI]"WinNT://$domain/$computername"
			Log "INFO: No credentials parameter."
		}

		write-host "Collecting local users..."

		Function  Build_SID {
			Param([byte[]]$BinarySID)
			(New-Object  System.Security.Principal.SecurityIdentifier($BinarySID,0)).Value
			Log "INFO: Build_SID function flag."
		}

		Function  Translate_Properties {
			Param  ($Property)
			$Properties  = New-Object  System.Collections.ArrayList
			Log "INFO: Properties variable flag."
			Switch  ($Property) {
				($Property  -BOR 0x0002)  {[void]$Properties.Add('ACCOUNTDISABLE')}
				($Property  -BOR 0x0010)  {[void]$Properties.Add('LOCKOUT')}
				($Property  -BOR 0x0020)  {[void]$Properties.Add('PASSWD_NOTREQD')}
				($Property  -BOR 0x0040)  {[void]$Properties.Add('PASSWD_CANT_CHANGE')}
				($Property  -BOR 0x10000)  {[void]$Properties.Add('DONT_EXPIRE_PASSWORD')}
				($Property  -BOR 0x800000)  {[void]$Properties.Add('PASSWORD_EXPIRED')}
			}
			$Properties  -join ','
		}

		$computer.psbase.children | where { $_.psbase.schemaClassName -eq 'user' } | foreach {
			$UserName = $_.Name[0]
			Log "INFO: UserName variable flag."
			$SID = Build_SID -BinarySID $_.ObjectSID[0]
			Log "INFO: SID variable flag."
			$PasswordAge = [math]::Round(($_.PasswordAge[0])/86400) #FAILS WITH LAUREATE with PS 2.0
			Log "INFO: PasswordAge variable flag."
			if ($_.LastLogin -is [System.Collections.CollectionBase]) { #FAILS WITH LAUREATE with PS 2.0
				$LastLogin = $_.LastLogin[0] }
			else {
				$LastLogin = "" }
			Log "INFO: LastLogin variable flag."
			$MinPasswordLength = $_.MinPasswordLength[0]
			Log "INFO: MinPasswordLength variable flag."
			$MinPasswordAge = [math]::Round(($_.MinPasswordAge[0])/86400)
			Log "INFO: MinPasswordAge variable flag."
			$MaxPasswordAge = [math]::Round(($_.MaxPasswordAge[0])/86400)
			Log "INFO: MaxPasswordAge variable flag."
			$BadPasswordAttempts = $_.BadPasswordAttempts[0]
			Log "INFO: BadPasswordAttempts variable flag."
			$MaxBadPasswordsAllowed = $_.MaxBadPasswordsAllowed[0]
			Log "INFO: MaxBadPasswordsAllowed variable flag."
			$Description = $_.Description[0]
			Log "INFO: Description variable flag."
			$AuthenticationType = $_.AuthenticationType #FAILS WITH LAUREATE with PS 2.0
			Log "INFO: AuthenticationType variable flag."
			$ACCOUNTDISABLE = "False"
			Log "INFO: ACCOUNTDISABLE variable flag."
			$LOCKOUT = "False"
			Log "INFO: LOCKOUT variable flag."
			$PASSWD_NOTREQD = "False"
			Log "INFO: PASSWD_NOTREQD variable flag."
			$PASSWD_CANT_CHANGE = "False"
			Log "INFO: PASSWD_CANT_CHANGE variable flag."
			$DONT_EXPIRE_PASSWORD = "False"
			Log "INFO: DONT_EXPIRE_PASSWORD variable flag."
			$PASSWORD_EXPIRED = "False"
			Log "INFO: PASSWORD_EXPIRED variable flag."
			$Bool_prop = Translate_Properties  -Property $_.UserFlags[0]		
			Log "INFO: Bool_prop variable flag."
			if ($Bool_prop -like '*ACCOUNTDISABLE*'){$ACCOUNTDISABLE = "True"}
			Log "INFO: ACCOUNTDISABLE condition flag."
			if ($Bool_prop -like '*LOCKOUT*'){$LOCKOUT = "True"}
			Log "INFO: LOCKOUT condition flag."
			if ($Bool_prop -like '*PASSWD_NOTREQD*'){$PASSWD_NOTREQD = "True"}
			Log "INFO: PASSWD_NOTREQD condition flag."
			if ($Bool_prop -like '*PASSWD_CANT_CHANGE*'){$PASSWD_CANT_CHANGE = "True"}
			Log "INFO: PASSWD_CANT_CHANGE condition flag."
			if ($Bool_prop -like '*DONT_EXPIRE_PASSWORD*'){$DONT_EXPIRE_PASSWORD = "True"}
			Log "INFO: DONT_EXPIRE_PASSWORD condition flag."
			if ($Bool_prop -like '*PASSWORD_EXPIRED*'){$PASSWORD_EXPIRED = "True"}
			Log "INFO: PASSWORD_EXPIRED condition flag."
			$new_local_user = "{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16}" -f $UserName, $SID, $PasswordAge, $LastLogin, $MinPasswordLength, $MinPasswordAge, $MaxPasswordAge, $BadPasswordAttempts, $MaxBadPasswordsAllowed, $Description, $AuthenticationType, $ACCOUNTDISABLE, $LOCKOUT, $PASSWD_NOTREQD, $PASSWD_CANT_CHANGE, $DONT_EXPIRE_PASSWORD, $PASSWORD_EXPIRED
			Log "INFO: new_local_user variable flag."	
			Add-Content -Encoding $output_encoding -Path $output_file_local_users -value $new_local_user 	
		}
		Log "INFO: Local users successfully collected."
	}
	catch{
		$errorFound = $True
		Log "ERROR: Local users collection procedure failed."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	###Collection of the local groups###############################################################################################################
	try{
		$ErrorActionPreference = 'Stop'

		$output_file_groups = New-Item -ItemType file -Path $ExtractPath/$computername"_localGroups.csv" -Force
		Log "INFO: output_file_groups variable flag."
		Set-Content "group full name;group short name;group sid" -Encoding $output_encoding -Path $ExtractPath/$computername"_localGroups.csv"
		Log "INFO: groups output file created."
		
		write-host "Collecting groups..."
		$computer.psbase.children | where { $_.psbase.schemaClassName -eq 'group' } | foreach {
			$local_group_full_name = $computername+"\"+$_.name
			Log "INFO: local_group_full_name variable flag."
			$local_group_short_name = New-Object System.Security.Principal.NTAccount($_.Name)
			Log "INFO: local_group_short_name variable flag."
			$sid = $local_group_short_name.Translate([System.Security.Principal.SecurityIdentifier]).Value
			Log "INFO: local_group_short_name variable flag."
			$new_local_group = "{0};{1};{2}" -f $local_group_full_name, $local_group_short_name, $sid
			Log "INFO: new_local_group variable flag."
			Add-Content -Encoding $output_encoding -Path $output_file_groups -value $new_local_group    
			}
		Log "INFO: Groups collection successfully ended."
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem collecting groups."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	###Collection of the groups members#############################################################################################################
	try{
		$ErrorActionPreference = 'Stop'

		$output_file = New-Item -ItemType file -Path $ExtractPath/$computername"_groupMembers.csv" -Force
		Log "INFO: output_file variable flag."
		Set-Content "group full name;group short name;group sid;member" -Encoding $output_encoding -Path $ExtractPath/$computername"_groupMembers.csv"
		Log "INFO: groups members output file created."
		
		write-host "Collecting groups members..."
		$computer.psbase.children | where { $_.psbase.schemaClassName -eq 'group' } | foreach {
			$group_full_name = $computername+"\"+$_.name
			Log "INFO: group_full_name variable flag."
			$group =[ADSI]$_.psbase.Path
			Log "INFO: group variable flag."
			$group_short_name = New-Object System.Security.Principal.NTAccount($_.Name)
			Log "INFO: group_short_name variable flag."
			$sid = $group_short_name.Translate([System.Security.Principal.SecurityIdentifier]).Value
			Log "INFO: group_short_name variable flag."
			$group.psbase.Invoke("Members") | foreach {
				$member = $_.getType().InvokeMember("ADSPath", 'GetProperty', $null, $_, $null).Replace("WinNT://","").Replace("/","\")
				Log "INFO: member variable flag."
				###BEGIN CORRECTION TEST
				$slashcount = ($member.split("\")).count -1
				Log "INFO: slashcount variable flag."
				if ($slashcount -gt 1){
					Log "INFO: slashcount -gt 1 flag."
					$posslash = $member.Indexof("\")
					Log "INFO: posslash variable flag."
					$member = $member.substring($posslash+1)
					Log "INFO: member variable flag."
				}
				###END CORRECTION TEST
				$new_member = "{0};{1};{2};{3}" -f $group_full_name, $group_short_name, $sid, $member
				Log "INFO: new_member variable flag."
				Add-Content -Encoding $output_encoding -Path $output_file -value $new_member    
			}
		}
		Log "INFO: Groups members collection successfully ended."
		
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem collecting groups members."
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}


	#Stop execution timer and display information########################################################################################################################
	try{
		$ErrorActionPreference = 'Stop'
		
		$globalWatch.Stop();
		Log "INFO: Timer successfully ended. Total execution time:" 
		Add-content $Logfile -value $globalWatch.Elapsed
	}
	catch{
		$errorFound = $True
		Log "ERROR: Problem stopping timer."	
		$ErrorMessage = $_.Exception.Message
		Log "	$ErrorMessage"
	}
	

	if ($Cred -ne [string]::empty){
		net use \\$computername /delete /yes
	}
	
	if ($errorFound){
		write-host "An error has occurred, please see the logs for more details." -ForegroundColor Red
		Log "ERROR: Script execution ended with error(s)."
		exit 2
	}
}


#Test if a servers list has been given as parameter, if not use the computername paramater as function input########################################################################################################################
if ($servers_list_file -eq [string]::Empty) {
	Collect_accounts($target_comp)
}
elseif ($servers_list_file -ne [string]::empty) {
	foreach($server in Get-Content $servers_list_file) {
		Collect_accounts($server)
	}
}


write-host "Extraction ended successfully."

exit 0
