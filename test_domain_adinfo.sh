#!/usr/bin/env bash
#
#
# -COPYRIGHT BRAINWAVE, all rights reserved.
#############


#$(realm list) >> realms_list.csv
#realm list >> realms_list.csv
adinfo >> adinfo.csv

# for i in $(getent passwd | cut -d: -f1) ; do
# 	id $i >> users_details.csv
# done

adquery user >> adquery_user_all.csv

for i in $(getent passwd | cut -d: -f1) ; do
	adquery user $i >> adquery_by_user.csv
done

for j in $(getent passwd | cut -d: -f1) ; do
	adquery user $j -A >> adquery_by_user_A.csv
done

adquery group >> adquery_group_all.csv

for k in $(getent group | cut -d: -f1) ; do
	adquery group $k >> adquery_by_group.csv
done

for l in $(getent group | cut -d: -f1) ; do
	adquery group $l -A >> adquery_by_group_A.csv
done


exit 0