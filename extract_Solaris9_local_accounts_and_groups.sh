#!/usr/bin/env bash
#
# 	DESCRIPTION
#		This script will create 5 files:
#		- The [COMPUTERNAME]_solaris_users.csv file that contains the local accounts information
#		- The [COMPUTERNAME]_solaris_groups.csv file with the information on the groups
#		- The [COMPUTERNAME]_solaris_groups_members.csv file which contains the list of groups' members
#		- The [COMPUTERNAME].name file which is used into the IGRC collector module (MANDATORY)
#		- The [COMPUTERNAME].log file, a log file
#
#	REQUIREMENTS
#		-Script must be executed with bash shell,
#		-The user that launches the script must have the rights to read, list and traverse the target files and the home directories of all users
#
#	USAGE
#		./extract_Solaris_local_accounts_and_groups.sh prefix [-o <outputDirectory>] [-g <logDirectory> ] [-e <logLevel> ] [-v] [-h]
#
#	PARAMETERS
#		-prefix: prefix for the extracted and generated files
#		-o <outputDirectory>: directory where extracted file will be stored, if not set extracted file will be in the current bash location
#		-g <logDirectory>: directory where log file will be stored, if not set extracted file will be in the current bash location
#		-e <logLevel>: supported values : debug, info (default), error
#		-v activates verbose mode, it overrides the logLevel and sets it to debug
#		-h show this help
#
#	SCRIPT RESULTS
#		The script will create 5 files into the output folder ([COMPUTERNAME]_solaris_users.csv, [COMPUTERNAME]_solaris_groups.csv, [COMPUTERNAME]_solaris_groups_members.csv, [COMPUTERNAME].name, [COMPUTERNAME].log)
#
#	EXAMPLE
#		./extract_Solaris_local_accounts_and_groups.sh SERVER1 -v
#			Will generate 5 files: SERVER1_solaris_users.csv, SERVER1_solaris_groups.csv, SERVER1_solaris_groups_members.csv, SERVER1.name, SERVER1.log)
#
#############

# use grep in /usr/xpg4/bin, which includes -q
PATH=/usr/xpg4/bin:$PATH
export PATH

function showhelp {
  echo ""
  echo "USAGE"
  echo "	./extract_Solaris_local_accounts_and_groups.sh prefix [-o <outputDirectory>] [-g <logDirectory> ] [-e <logLevel> ] [-v] [-h]"
  echo ""
  echo "PARAMETERS"
  echo "	-prefix: prefix for the extracted and generated files"
  echo "	-o <outputDirectory>: directory where extracted file will be stored, if not set extracted file will be in the current bash location"
  echo "	-g <logDirectory>: directory where log file will be stored, if not set extracted file will be in the current bash location"
  echo "	-e <logLevel>: supported values : debug, info (default), error"
  echo "	-v activates verbose mode, it overrides the logLevel and sets it to debug"
  echo "	-h show this help"
  echo ""
  echo "SCRIPT RESULTS"
  echo "	The script will create 5 files into the output folder ([COMPUTERNAME]_solaris_users.csv, [COMPUTERNAME]_solaris_groups.csv, [COMPUTERNAME]_solaris_groups_members.csv, [COMPUTERNAME].name, [COMPUTERNAME].log)"
  echo ""
  echo "EXAMPLE"
  echo "	./extract_Solaris_local_accounts_and_groups.sh SERVER1 -v"
  echo "		Will generate 5 files: SERVER1_solaris_users.csv, SERVER1_solaris_groups.csv, SERVER1_solaris_groups_members.csv, SERVER1.name, SERVER1.log)"
  echo ""
}

function seq { 
    i=$1;
    while [ $i -le $2 ]; do
        printf "%d " $i
        i=`expr $i + 1`
    done
}

function logDebug {
	message="[$(date -u +"%Y %m %d %H:%M:%S")] Debug : $1"
	if [[ "$3" == "debug" ]]; then
    echo "$message"
		echo "$message">>"$2"
	fi
}

function logMessage {
	message="[$(date -u +"%Y %m %d %H:%M:%S")] Info : $1"
	echo "$message"
	if [[ "$3" == "info" ]] || [[ "$3" == "debug" ]]; then
		echo "$message">>"$2"
	fi
}

function logError {
	message="[$(date -u +"%Y %m %d %H:%M:%S")] Error : $1"
	echo "$message"
	if [[ "$3" == "error" ]] || [[ "$3" == "info" ]] || [[ "$3" == "debug" ]]; then
		echo "$message">>"$2"
	fi
}

function failAndExit {
  logError "$1" "$2" "$3"
  exit 1
}

# Lets read the parameters
positionalParams=()
# Here the default values
option_outputdir="."
option_logdir="."
option_loglevel="info"
while [ $# -gt 0 ] && [ "$1" != "--" ]; do
  # Read options
  while getopts "o:g:e:v:h" option; do
    case $option in
    o)
        if [[ -d "$OPTARG" ]]; then
          option_outputdir=$OPTARG
        else
          echo "Value of option -o is not a valid directory"
          exit 1
        fi
    ;;
    g)
        if [[ -d "$OPTARG" ]]; then
          option_logdir=$OPTARG
        else
          echo "Value of option -g is not a valid directory"
          exit 1
        fi
    ;;
    e)
        if [[ "$OPTARG" == "debug" ]]; then
          option_loglevel="debug"
        else
          if [[ "$OPTARG" == "info" ]]; then
            option_loglevel="info"
          else
            if [[ "$OPTARG" == "error" ]]; then
              option_loglevel="error"
            else
              echo "Value of option -e is not valid (error, info, debug)"
              exit 1
            fi
          fi
        fi
    ;;
    v)
        option_loglevel="debug"
    ;;
    h)
        showhelp
        exit 0
    ;;
    esac
  done
  shift $((OPTIND-1))
  # Read required parameters
  #while [ $# -gt 0 ] && ! [[ "$1" =~ ^- ]]; do
  while [ $# -gt 0 ] && ! expr "$1" : "^-" ; do
    positionalParams=("${positionalParams[@]}" "$1")
    shift
  done
done

# 1 parameter is required
if [[ "${#positionalParams[@]}" -ne 1 ]]; then
  echo "Invalid number of parameters"
  showhelp
  exit 1
fi

# Get the prefix
param_prefix="${positionalParams[0]}"

# Check that all required commands are available
required="1"
if ! hash bash 2>/dev/null ; then
	required="0"
fi
if ! hash cp 2>/dev/null ; then
	required="0"
fi
if ! hash cut 2>/dev/null ; then
	required="0"
fi
if [[ "$required" -eq "0" ]]; then
	echo "One or more required commands are not available"
	exit 2
fi

# Global vars
logfile="${option_logdir}/${param_prefix}_extract_Solaris_local_accounts_and_groups_$(date +"%Y_%m_%d_%H%M%S").log"

# Lets start
logMessage "Starting extraction" "$logfile" "$option_loglevel"

# Remove outputfiles if they exist
rm -f $option_outputdir/${param_prefix}_solaris_users.csv
rm -f $option_outputdir/${param_prefix}_solaris_groups.csv
rm -f $option_outputdir/${param_prefix}_solaris_groups_members.csv
rm -f $option_outputdir/${param_prefix}_groups_members_temp.csv
rm -f $option_outputdir/${param_prefix}_groups_members_withdup.csv
rm -f $option_outputdir/${param_prefix}_chage_attributes.txt
rm -f $option_outputdir/${param_prefix}_superuser.txt
rm -f $option_outputdir/${param_prefix}_lastco.txt


# Print parameters values (debug mode)
logDebug "param_prefix = $param_prefix" "$logfile" "$option_loglevel"
logDebug "option_outputdir = $option_outputdir" "$logfile" "$option_loglevel"
logDebug "option_logdir = $option_logdir" "$logfile" "$option_loglevel"
logDebug "option_loglevel = $option_loglevel" "$logfile" "$option_loglevel"


######Test access to some mandatory commands and files######
testgetentgroup=$(getent group) 2>> "$logfile" || failAndExit "'getent group' command is not available. This command is mandatory to run this script. Please check if you have the right to launch this command." "$logfile" "$option_loglevel"
logDebug "Flag: test getent group command" "$logfile" "$option_loglevel"
testgetentpasswd=$(getent passwd) 2>> "$logfile" || failAndExit "'getent passwd' command is not available. This command is mandatory to run this script. Please check if you have the right to launch this command." "$logfile" "$option_loglevel"
logDebug "Flag: test getent passwd command" "$logfile" "$option_loglevel"
testetcshadow=$(cp /etc/shadow $option_outputdir/${param_prefix}_testshadow.txt) 2>> "$logfile" || failAndExit "Access to '/etc/shadow' file is denied. This access is mandatory to run this script. Please check if you have the correct right to read this file." "$logfile" "$option_loglevel"
logDebug "Flag: test /etc/shadow file access" "$logfile" "$option_loglevel"
rm -f $option_outputdir/${param_prefix}_testshadow.txt


#####Additionnal tests############################
# test if /etc/sudoers exists
sudoers_file=""
[ -f /etc/sudoers ] && sudoers_file="true" || sudoers_file="false"
logDebug "Flag: test /etc/sudoers file access/existence" "$logfile" "$option_loglevel"

if [[ "$sudoers_file" == "true" ]] ; then
	testetcsudoers=$(cp /etc/sudoers $option_outputdir/${param_prefix}_testsudoers.txt) 2>> "$logfile" || failAndExit "Access to '/etc/sudoers' file is denied. This access is mandatory to run this script. Please check if you have the correct right to read this file." "$logfile" "$option_loglevel"
	logDebug "Flag: test /etc/sudoers file access/existence (if)" "$logfile" "$option_loglevel"
	rm -f $option_outputdir/${param_prefix}_testsudoers.txt
fi


# test if /usr/bin/last exists
last_cmd_file=""
[ -f /usr/bin/last ] && last_cmd_file="true" || last_cmd_file="false"
logDebug "Flag: test /usr/bin/last file access/existence" "$logfile" "$option_loglevel"


######create output_files with the right columns######
echo "user_name;user_ID;user_home_directory;user_command_interpreter;last_passwd_change;passwd_expires;password_inactive;account_expires;min_days_between_passwd_change;max_days_between_passwd_change;days_of_warning_before_passwd_expires;account_locked;passwd_not_required;last_login_date;superuser" >> $option_outputdir/${param_prefix}_solaris_users.csv
echo "group_name;group_ID;superuser_group" >> $option_outputdir/${param_prefix}_solaris_groups.csv
echo "group_name;group_ID;member" >> $option_outputdir/${param_prefix}_groups_members_temp.csv
echo "" >> $option_outputdir/${param_prefix}.name
logDebug "Flag: create output files" "$logfile" "$option_loglevel"


######create variables######
declare -a username_array
declare -a userID_array
declare -a userdescription_array
declare -a userhomedirectory_array
declare -a usercommandinterpreter_array
declare -a userprimarygroupname_array
declare -a userprimarygroupID_array
declare -a last_passwd_change_array
declare -a passwd_expires_array
declare -a password_inactive_array
declare -a account_expires_array
declare -a min_days_between_passwd_change_array
declare -a max_days_between_passwd_change_array
declare -a days_of_warning_before_passwd_expires_array
declare -a groupname_array
declare -a groupID_array
declare -a groupmembers_array
declare -a account_locked_array
declare -a passwd_not_required_array
declare -a last_login_date_array
declare -i count=0
declare -i secondcount=0
declare -i loop_csv
declare -a SPLIT_array
declare -a login_date_SPLIT_array
declare -a superuser_array
declare -a superuser_group_array
declare -a grep_lines_array
logDebug "Flag: create variables" "$logfile" "$option_loglevel"


######collect superusers accounts######
awk -F':' '{if($3 == 0) print $1}' /etc/passwd >>  $option_outputdir/${param_prefix}_superuser.txt


##############GROUPS###############################################################
count=0
##Fill in arrays basing on groupname
logDebug "Flag: start groups extraction" "$logfile" "$option_loglevel"
for i in $(getent group | cut -d: -f1) ; do
    groupname_array[$count]=$(getent group $i | cut -d: -f1)
    logDebug "Flag: groupname_array variable" "$logfile" "$option_loglevel"
	groupID_array[$count]=$(getent group $i | cut -d: -f3)
	logDebug "Flag: groupID_array variable" "$logfile" "$option_loglevel"

	##Detect if the group is a superusers group
	if grep -q "%$i" $option_outputdir/${param_prefix}_superuser.txt ; then
		superuser_group_array[$count]="True"
		logDebug "Flag: superuser_group_array variable (true)" "$logfile" "$option_loglevel"
	else
		superuser_group_array[$count]="False"
		logDebug "Flag: superuser_group_array variable (false)" "$logfile" "$option_loglevel"
	fi
	let "count= $count + 1"
done

count=count-1
##Fill in output files
for j in $(seq 0 $count) ; do
	{ echo -n "${groupname_array[j]}";echo -n ";";echo -n "${groupID_array[j]}";echo -n ";";echo "${superuser_group_array[j]}"; } >> $option_outputdir/${param_prefix}_solaris_groups.csv
	logDebug "Flag: fill groups output file" "$logfile" "$option_loglevel"
done
logMessage "Groups successfully extracted" "$logfile" "$option_loglevel"


##############USERS###############################################################
count=0
##Fill in arrays basing on username
logMessage "Start users extraction" "$logfile" "$option_loglevel"
for i in $(getent passwd | cut -d: -f1) ; do
  	username_array[$count]=$(getent passwd $i | cut -d: -f1)
  	logDebug "Flag: username_array variable" "$logfile" "$option_loglevel"
	userID_array[$count]=$(getent passwd $i | cut -d: -f3)
	logDebug "Flag: userID_array variable" "$logfile" "$option_loglevel"
	userhomedirectory_array[$count]=$(getent passwd $i | cut -d: -f6)
	logDebug "Flag: userhomedirectory_array variable" "$logfile" "$option_loglevel"
	usercommandinterpreter_array[$count]=$(getent passwd $i | cut -d: -f7)
	logDebug "Flag: usercommandinterpreter_array variable" "$logfile" "$option_loglevel"
	userprimarygroupID_array[$count]=$(getent passwd $i | cut -d: -f4)
	logDebug "Flag: userprimarygroupID_array variable" "$logfile" "$option_loglevel"

####################	
    current_user=${username_array[$count]}
	usrstatus=$(passwd -s $current_user | awk '{print $2}' )
	usrlk='LK'
	usrps='PS'
	usrnp='NP'
	usrup='UP'
	usrnl='NL'
	
	lastsesion=$(last | grep -w $current_user | head -1 | awk '{print $4" "$5" "$6" "$7}')
	
	laschangepassnum=$(cat /etc/shadow | awk -F':' '{print $1" "$3}' | grep -w $current_user | awk '{print $2*86400+21600}')
	last_passwd_change_array[$count]=$(perl -MPOSIX -le 'print POSIX::strftime("%b %d, %Y", localtime $ARGV[-1])' $laschangepassnum)
	logDebug "Flag: last_passwd_change_array variable" "$logfile" "$option_loglevel"
	
	passexpirenum=$(cat /etc/shadow | awk -F':' '{print $1" "$8}' | grep -w $current_user | awk '{print $2*86400+21600}')
	passexpire=$(perl -MPOSIX -le 'print POSIX::strftime("%b %d, %Y", localtime $ARGV[-1])' $passexpirenum)
	passexpiredate="Jan 01, 1970"
	
	minpasschange=$(cat /etc/shadow | awk -F':' '{print $1" "$4}' | grep -w $current_user | awk '{print $2}')
	numpassexpire=$(cat /etc/shadow | awk -F':' '{print $1" "$5}' | grep -w $current_user | awk '{print $2}')
	
	umbpass=$(cat /etc/shadow | awk -F':' '{print $1" "$6}' | grep -w $current_user | awk '{print $2}')
	
	if  [ $usrstatus = $usrlk ] || [ $usrstatus = $usrup ] 2>/dev/null
    then
		password_inactive_array[$count]=$passexpire    
	else
		password_inactive_array[$count]="False"
	fi
	logDebug "Flag: password_inactive_array variable" "$logfile" "$option_loglevel"	

	if  [ $passexpire = $passexpiredate ] 2>/dev/null       
	then
		passwd_expires_array[$count]="False"
		account_expires_array[$count]="False"
	 else
		passwd_expires_array[$count]=$passexpire
		account_expires_array[$count]=$passexpire
	fi
	logDebug "Flag: passwd_expires_array variable" "$logfile" "$option_loglevel"
	logDebug "Flag: account_expires_array variable" "$logfile" "$option_loglevel"

	
	if  [ -z $umbpass ] 2>/dev/null 
	then
		days_of_warning_before_passwd_expires_array[$count]=0
		days_of_warning_before_passwd_expires_array[$count]=$umbpass
	fi
	logDebug "Flag: days_of_warning_before_passwd_expires_array variable" "$logfile" "$option_loglevel"


	if  [ -z $minpasschange ] 2>/dev/null   
	then
		min_days_between_passwd_change_array[$count]=0
	else
		min_days_between_passwd_change_array[$count]=$minpasschange
	fi
	logDebug "Flag: min_days_between_passwd_change_array variable" "$logfile" "$option_loglevel"


	if  [ -z $numpassexpire ] 2>/dev/null   
	then
		max_days_between_passwd_change_array[$count]=9999
	else
	    max_days_between_passwd_change_array[$count]=$numpassexpire
	fi
	logDebug "Flag: max_days_between_passwd_change_array variable" "$logfile" "$option_loglevel"


	passwd_status=$(grep $i /etc/shadow | cut -d: -f2)
	logDebug "Flag: passwd_status variable" "$logfile" "$option_loglevel"
	if [[ "$passwd_status" == "*" || "$passwd_status" == "!!" || "$passwd_status" == "!" || "$passwd_status" == "*LK*" ]] ; then
		account_locked_array[count]="True"
		logDebug "Flag: account_locked_array variable (if)" "$logfile" "$option_loglevel"
		passwd_not_required_array[count]="False"
		logDebug "Flag: passwd_not_required_array variable (if)" "$logfile" "$option_loglevel"
	elif [[ "$passwd_status" == "" ]] ; then
		account_locked_array[count]="False"
		logDebug "Flag: account_locked_array variable (elif)" "$logfile" "$option_loglevel"
		passwd_not_required_array[count]="True"
		logDebug "Flag: passwd_not_required_array variable (elif)" "$logfile" "$option_loglevel"
	else
		account_locked_array[count]="False"
		logDebug "Flag: account_locked_array variable (else)" "$logfile" "$option_loglevel"
		passwd_not_required_array[count]="False"
		logDebug "Flag: passwd_not_required_array variable (else)" "$logfile" "$option_loglevel"
	fi

	##Detect if the user is a superuser
	if grep -q "$i" $option_outputdir/${param_prefix}_superuser.txt ; then
		superuser_array[$count]="True"
		logDebug "Flag: superuser_array variable (True)" "$logfile" "$option_loglevel"
	else
		superuser_array[$count]="False"
		logDebug "Flag: superuser_array variable (False)" "$logfile" "$option_loglevel"
	fi


	if [[ "$last_cmd_file" == "true" ]] ; then
		echo $(last -n 1  $i | perl -p -e 's/(\S+)\s+(\S+)\s+\S+\s+(.*)/$1 $2 $3/;') >> $option_outputdir/${param_prefix}_lastco.txt
		logDebug "Flag: fill lastco temp file" "$logfile" "$option_loglevel"
	else
		echo " " >> $option_outputdir/${param_prefix}_lastco.txt
		logDebug "Flag: do not fill lastco temp file" "$logfile" "$option_loglevel"
	fi


	if [[ "$(sed '1!d' $option_outputdir/${param_prefix}_lastco.txt)" == "" || "$(sed '1!d' $option_outputdir/${param_prefix}_lastco.txt)" == "wtmp"* ]] ; then
		last_login_date_array[count]=""
		logDebug "Flag: last_login_date_array variable (if)" "$logfile" "$option_loglevel"
	else
		unset login_date_SPLIT_array[@]
		logDebug "Flag: unset" "$logfile" "$option_loglevel"
		#read -ra login_date_SPLIT_array <<< "$(sed '1!d' $option_outputdir/${param_prefix}_lastco.txt)"
		for x in $(sed '1!d' $option_outputdir/${param_prefix}_lastco.txt)
		do
			login_date_SPLIT_array=("${login_date_SPLIT_array[@]}" "$x")
		done
		logDebug "Flag: read" "$logfile" "$option_loglevel"
		last_login_date_array[count]="${login_date_SPLIT_array[2]} ${login_date_SPLIT_array[3]} ${login_date_SPLIT_array[4]}"
		logDebug "Flag: last_login_date_array variable (else)" "$logfile" "$option_loglevel"
	fi
	rm -f $option_outputdir/${param_prefix}_lastco.txt


	##Place each user in his primary group into the groupsmembers file
	secondcount=0
	for k in "${groupID_array[@]}" ; do
		if (( $k ==  userprimarygroupID_array[$count] )) ; then
			userprimarygroupname_array[$count]=${groupname_array[$secondcount]}	
			logDebug "Flag: userprimarygroupname_array variable" "$logfile" "$option_loglevel"
			break
		else
			let "secondcount= $secondcount + 1"
		fi
	done
	let "count= $count + 1"
 done

count=count-1

##Fill in the ouput files (users and groupsmembers only with primarygroup)
for j in $(seq 0 $count) ; do
	{ echo -n "${username_array[j]}";echo -n ";";echo -n "${userID_array[j]}";echo -n ";";echo -n "${userhomedirectory_array[j]}";echo -n ";";echo -n "${usercommandinterpreter_array[j]}";echo -n ";";echo -n "${last_passwd_change_array[j]}";echo -n ";";echo -n "${passwd_expires_array[j]}";echo -n ";";echo -n "${password_inactive_array[j]}";echo -n ";";echo -n "${account_expires_array[j]}";echo -n ";";echo -n "${min_days_between_passwd_change_array[j]}";echo -n ";";echo -n "${max_days_between_passwd_change_array[j]}";echo -n ";";echo -n "${days_of_warning_before_passwd_expires_array[j]}";echo -n ";";echo -n "${account_locked_array[j]}";echo -n ";";echo -n "${passwd_not_required_array[j]}";echo -n ";";echo -n "${last_login_date_array[j]}";echo -n ";";echo "${superuser_array[j]}"; } >> $option_outputdir/${param_prefix}_solaris_users.csv
	logDebug "Flag: fill users output file" "$logfile" "$option_loglevel"
	{ echo -n "${userprimarygroupname_array[j]}";echo -n ";";echo -n "${userprimarygroupID_array[j]}";echo -n ";";echo "${username_array[j]}"; } >> $option_outputdir/${param_prefix}_groups_members_temp.csv
	logDebug "Flag: fill groups_members output file (temp)" "$logfile" "$option_loglevel"
done
rm -f $option_outputdir/${param_prefix}_superuser.txt
logMessage "Users successfully extracted" "$logfile" "$option_loglevel"


##############GROUPS_MEMBERS###############################################################
count=0
##Fill in arrays basing on groupname
for i in $(getent group | cut -d: -f1) ; do
	groupmembers_array[$count]=$(awk -F':' '/'$i'/{print $4}' /etc/group)
	logDebug "Flag: groupmembers_array variable" "$logfile" "$option_loglevel"

	##Fill in a temp groupmembers file which will be reshaped later to delete duplicated members
	if [ "$(awk -F':' '/'$i'/{print $4}' /etc/group)" != "" ] ; then
		if [[ ${groupmembers_array[count]} == *","* ]] ; then
			unset SPLIT_array[@]
			logDebug "Flag: unset" "$logfile" "$option_loglevel"
			IFS=', '
			#read -ra SPLIT_array <<< ${groupmembers_array[count]}
			for x in ${groupmembers_array[count]}
			do
				SPLIT_array=("${SPLIT_array[@]}" "$x")
			done
			logDebug "Flag: split" "$logfile" "$option_loglevel"
			for j in "${SPLIT_array[@]}" ; do
				{ echo -n "${groupname_array[count]}";echo -n ";";echo -n "${groupID_array[count]}";echo -n ";";echo "$j"; } >> $option_outputdir/${param_prefix}_groups_members_temp.csv
				logDebug "Flag: fill groups_members output file (temp bis)" "$logfile" "$option_loglevel"
			done
			IFS=' '
		else
			{ echo -n "${groupname_array[count]}";echo -n ";";echo -n "${groupID_array[count]}";echo -n ";";echo "${groupmembers_array[count]}"; } >> $option_outputdir/${param_prefix}_groups_members_temp.csv
			logDebug "Flag: fill groups_members output file (temp ter)" "$logfile" "$option_loglevel"
		fi
	fi
	let "count= $count + 1"
done
logMessage "Groups members successfully extracted" "$logfile" "$option_loglevel"


##############RESHAPE OUTPUT FILES###############################################################
#Remove extra usernames (alone on their line)
sed '/[;]/!d' $option_outputdir/${param_prefix}_groups_members_temp.csv >> $option_outputdir/${param_prefix}_groups_members_withdup.csv
logDebug "Flag: fill groups_members output file (with dup)" "$logfile" "$option_loglevel"
rm -f $option_outputdir/${param_prefix}_groups_members_temp.csv

#Remove duplicated lines
awk '!seen[$0]++' $option_outputdir/${param_prefix}_groups_members_withdup.csv >> $option_outputdir/${param_prefix}_solaris_groups_members.csv
logDebug "Flag: fill groups_members output file" "$logfile" "$option_loglevel"
rm -f $option_outputdir/${param_prefix}_groups_members_withdup.csv
logMessage "Output files successfully reshaped" "$logfile" "$option_loglevel"


logMessage "Finished Successfully" "$logfile" "$option_loglevel"
exit 0
