#!/usr/bin/env bash
#
#
# -COPYRIGHT BRAINWAVE, all rights reserved.
#############


#$(realm list) >> realms_list.csv
realm list >> realms_list.csv


for i in $(getent passwd | cut -d: -f1) ; do
	id $i >> users_details.csv
done

exit 0
